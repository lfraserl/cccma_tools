#! /bin/sh

#    Nov 14/13 - F.Majaess

#id  sumflsz - Computes size total sum of normal local files

#    AUTHOR  - F.Majaess

#hd  PURPOSE - "sumflsz" script is used to compute the total
#hd            sum of the size of all normal local files in 
#hd            the current subdirectory. 
#hd            Note: Mainly the script is used by "spltall"
#hd                  program.
#hd                  if ".VARTABLE" file is present, the script
#hd                  confines itself to the filename entries
#hd                  found in it. Otherwise, the list of all
#hd                  "normal" files is generated via 
#hd                  "\ls -ld *" in the local subdirectory
#hd                  filtering out entries associated with
#hd                  any links/subdirectories.
#hd
#hd                  The script can also optionally verify 
#hd                  the computed sum versus specified input 
#hd                  value.

#pr  PARAMETERS:
#pr
#pr    POSITIONAL
#pr
#pr      arg1   = Optional expected input value to verify the 
#pr               computed size total sum of the local normal
#pr               files against.
#pr

#ex  EXAMPLE:
#ex
#ex          sumflsz 
#ex
#ex    The above will result in reporting back the computed
#ex    sum of the size of all local files listed in, .VARTABLE
#ex    if present, or from invoking "ls -ld *" from within the 
#ex    current subdirectory with entries associated with 
#ex    links/subdirectories excluded. 
#ex
#ex          sumflsz 123456
#ex
#ex    The script will compute the total sum of the file sizes
#ex    and verify it vs. "123456" expected sum value.
#ex

if [ $# -gt 1 ] ; then
 echo ' --------------' ;  echo " sumflsz: What was invoked:"
 echo " $0 $@"
 echo ' "sumflsz" expects an optional maximum of 1 argument to be specified!'
 echo ' -------------'
 exit 1
elif [ $# -lt 1 ] ; then
 AWK=${AWK:='awk'}
 # echo ' --------------' ;  echo " sumflsz: What was invoked:"
 # echo " $0 $@"
 if [ -s "./.VARTABLE" ] ; then
 echo " Using ./.VARTABLE filename entries."
  computed_filesizes=`cat ./.VARTABLE | $AWK '{ print " " $1 ; }' | tr '\012' '\040' | sed -e 's/  */ /g' | sed -e 's/^/\\\ls -ld /' |  /bin/sh - | egrep -v '^l|^d' | $AWK ' BEGIN { total=0 ;} ; { total=total+$5 ; } ; END { printf ( "%.0f\n", total ) ; } '`
 else
  computed_filesizes=`\ls -ld * | egrep -v '^l|^d' | $AWK ' BEGIN { total=0 ;} ; { total=total+$5 ; } ; END { printf ( "%.0f\n", total ) ; } '`
 fi
 echo "$computed_filesizes"
 # echo ' -------------'
 exit 0
elif [ $# -eq 1 ] ; then
 AWK=${AWK:='awk'}
 echo ' --------------' ;  echo " sumflsz: What was invoked:"
 echo " $0 $@"
 expected_filesizes="$1"
 if [ -s "./.VARTABLE" ] ; then
 echo " Using ./.VARTABLE filename entries."
  computed_filesizes=`cat ./.VARTABLE | $AWK '{ print " " $1 ; }' | tr '\012' '\040' | sed -e 's/  */ /g' | sed -e 's/^/\\\ls -ld /' | /bin/sh - | egrep -v '^l|^d' | $AWK ' BEGIN { total=0 ;} ; { total=total+$5 ; } ; END { printf ( "%.0f\n", total ) ; } '`
 else
  computed_filesizes=`\ls -ld * | egrep -v '^l|^d' | $AWK ' BEGIN { total=0 ;} ; { total=total+$5 ; } ; END { printf ( "%.0f\n", total ) ; } '`
 fi
 echo " sumflsz: expected_filesizes=$expected_filesizes, computed_filesizes=$computed_filesizes"
 if [ "$computed_filesizes" -eq "$expected_filesizes" ] ; then
  echo ' sumflsz: Check passed; computed value matches the expected total size of targeted files'
  echo ' -------------'
  exit 0
 else
  echo " sumflsz: Mismatched sizes!"
  echo ' -------------'
  exit 1
 fi
fi
