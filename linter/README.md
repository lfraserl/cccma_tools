# Fortran Adaptive Standardization Tool (FAST)

This tool is made for linting Fortran source code files, and modifying them to fit the [style guide outlined by CCCma](https://canesm.readthedocs.io/en/latest/fortran-update.html).

## Getting Started

The prerequisites for running this tool are access to python version 3.6 and the [pyyaml library](https://pypi.org/project/PyYAML/) -
an example [conda](https://docs.conda.io/projects/conda/en/latest/user-guide/getting-started.html) environment file has been provided
at the same location as this README.

Once the prerequisites are met, there are three ways to invoke the linter:

* Standard linting
* Fixing files to conform to the aforementioned standard
* Restoring original files that were modified with the fixer

## Linting

This feature is primarily intended to be used within a CI pipeline. The linter should be invoked from the root directory of the files to be scanned. This will usually mean the root of a repository such as CanAM. The linter will then search for a `linter.yml` configuration file, which tells the linter which files to ignore and which rules to fail on. A sample configuration file is given in this repository.

To invoke the linter, simply run the base script:

```
python3 ../path/to/fast.py
```

## Fixing

If passed the `--fix [FILES]` argument, this tool will parse the files, fix many syntactical deviations, and leave a backup of the original file in a `backups` folder on the same path.

Relative paths and absolute paths are both acceptable. Wildcards are properly parsed in the arguments:

```
python3 ../path/to/fast.py --fix path/to/files/*
```

If used on a fixed-form `.f/.F` file, the new file will be a `.f90` extension. Keep this in mind when compiling the code.

## Restoring Original Files

In case of unintended program behaviour, or a desire to return to the original files, simply call the program as above in the 'Fixing' section, but replace the `--fix` argument with the `--restore` argument.

```
python3 ../path/to/fast.py --restore path/to/files/*
```

This will delete the corrected file, restore the original from the `backups` folder, and delete the `backups` folder if no files remain in there.
