from fabric import Connection
from invoke.exceptions import UnexpectedExit
from invoke import Responder
from datetime import datetime
import os
import re
import shutil
from typing import Dict, Union
from canesm.exceptions import wrap_canesm_remotefail, RemoteError
import canesm
from canesm.util import (RemoteFile, RemoteDBConn, ProcessBash, ProcessCPPDef, ProcessString,
                         year_from_time, month_from_time, previous_month, add_time, is_null)
from typing import Type, List
import logging
import pandas as pd


class CanESMsetup:
    """
    Class that handles the setup and running of one instance of the CanESM model on a remote machine.

    Parameters
    ----------
    ver :
        git hash or git branch that will used to pull the code
    config :
        `AMIP` or `ESM`
    runid :
        name of the run
    repo :
        canesm repo address to clone from 
    user :
        User name on the machine where the job will be ran
    run_directory :
        Directory name where the code will be stored
    machine :
        Name of the machine where the job is run

    Examples
    --------
    >>> esm = CanESMsetup(ver='develop-canesm', config='AMIP', runid='testrun',
    ...                   user='raa000', run_directory='test_folder', machine='hare')
    >>> esm.start_time = 2000
    >>> esm.stop_time = 2100
    >>> esm.canesm_cfg['gpxsave'] = 'on'
    >>> esm.restart_files['runid_in'] = 'vsa_v4_01'
    >>> esm.restart_files['date_in'] = '1989_m12'
    >>> esm.tapeload = True
    >>> esm.setup_job()
    >>> esm.submit_job()

    NOTE:
        - the ensemble tool makes a bunch of sequencer/config system specific assumptions
            (i.e. what tools are used to configure, compile, and launch the runs)
          While we don't currently support multiple sequencers within the same version,
          it would be good to think on how to keep things separate, so when the
          sequencer is updated/changed, it is more obvious what to change.
    """

    def __init__(self, ver: str = '', config: str = '', runid: str = '', repo: str = '',
                 user: str = '', run_directory: str = '', machine: str = ''):

        # machine and user specifications
        self.machine = machine
        self.run_directory = run_directory  # directory where the code is installed
        self.namelist_directory = os.path.join(run_directory, "config/namelists")
        self.user = user  # user name used for ssh connections

        # canesm run setup
        self.config = config
        self.ver = ver
        self.repo = repo
        self.runid = runid
        self.start_time = 2003
        self.stop_time = 2008
        self.tapeload = False
        self.setup_flags = None

        # namelists to pull from restarts at setup
        self.namelists_to_pull_from_restarts = {}

        # dictionaries containing file-specific setup changes
        self.canesm_cfg = {}
        self.namelist_mods = {}
        self.restart_files = {}
        self.cpp_defs = {}

        self.is_setup = False
        self.responders = [Responder(pattern=r'\*\*\* Use .* option to use .* in the local run directory. \*\*\*',
                                     response='\n'),
                           Responder(pattern=r'WARNING: directory .* exists!',
                                     response='y\n'),
                           Responder(pattern=r'WARNING: year_rtdiag_start=.* != start_year=.* or/and'
                                             r' month_rtdiag_start=.* != start_month=.* !!!',
                                     response='y\n')]

        self._runpath = None
        self._ccrnsrc = None
        self._wrk_dir = None
        self.logger = logging.getLogger('canesm-ensemble')

    @property
    def start_year(self) -> int:
        return year_from_time(self.start_time)

    @property
    def start_month(self) -> int:
        return month_from_time(self.start_time, default_month=1)

    @property
    def stop_year(self) -> int:
        return year_from_time(self.stop_time)

    @property
    def stop_month(self) -> int:
        return month_from_time(self.stop_time, default_month=12)

    @property
    def runpath(self) -> str:
        if self._runpath is None:
            result = self.run_command('echo $RUNPATH')
            self._runpath = result.stdout.strip()
        return self._runpath

    @property
    def ccrnsrc(self) -> str:
        if self._ccrnsrc is None:
            result = self.run_command('echo $CCRNSRC')
            self._ccrnsrc = result.stdout.strip()
        return self._ccrnsrc

    @property
    def wrk_dir(self) -> str:
        if self._wrk_dir is None:
            result = self.run_command('echo $WRK_DIR')
            self._wrk_dir = result.stdout.strip()
        return self._wrk_dir

    @property
    def events(self) -> pd.DataFrame:
        """
        return the history of events
        """

        filename = os.path.join(self.ccrnsrc, '..', self.runid + '-log.db')
        with RemoteDBConn(filename, machine=self.machine, user=self.user) as db:
            data = pd.read_sql_query('SELECT * FROM events', db)

        return data

    @property
    def queue_files(self) -> List[str]:
        """
        return a list of files from the .queue directory associated with this run
        """
        files = self.run_command('ls ~/.queue', setup_run_env=False, run_directory='~').stdout.strip().split('\n')

        qfiles = []
        for file in files:
            if self.runid in file:
                qfiles.append(file)
        return qfiles

    def load_file(self, file, directory: str = ''):
        """
        return the contents of a file from the remote machine

        Parameters
        ----------
        file :
            Name of the file in the queue directory
        directory :
            Name of the directory where file is located, defaults to ~/.queue

        """

        file = os.path.join(directory, file)
        try:
            with RemoteFile(file, machine=self.machine, user=self.user) as f:
                contents = f.read()
        except FileNotFoundError:
            contents = None

        return contents

    @wrap_canesm_remotefail
    def _process_file(self, file: str, settings: Dict[str, Union[str, int, float]],
                      src_dir: str = None, processor: Type[ProcessString] = ProcessBash):
        """
        Update the file with the new settings. Options specified by the dictionary key will be set with the value.

        Parameters
        ----------
        file :
            name of the file to be processed
        settings :
            A dictionary of options that will changed in the file
        src_dir :
            The directory of the file. If not provided it is assumed to be `self.run_directory`
        processor:
            The string processor used to replace parameter values
        """
        if len(settings.keys()) == 0:
            return

        if src_dir is None:
            src_dir = self.run_directory

        # make local copy of the file, edit it, and put it back on remote
        with RemoteFile(os.path.join(src_dir, file), self.machine, self.user, mode='r') as f:
            file_contents = f.read()
        with RemoteFile(os.path.join(src_dir, file), self.machine, self.user, mode='w') as f:
            f.write(processor(file_contents).process(settings))

    def _run_setup_command(self, link_source: bool = False, source_to_link_against: str = None):
        """
        Use the setup-canesm script on the remote machine to download the code from the gitlab repository
        """
        if self.config == '' or self.runid == '':
            raise ValueError('config and runid must be specified before running')
        setup_exec = self.get_setup_canesm_exec()

        # construct setup command depending on if we are linking or not
        if link_source:
            if source_to_link_against is None:
                raise ValueError('a repo to link against must be provided if link_source is True')
            else:
                setup_command = f"{setup_exec} config={self.config} runid={self.runid} fetch_method=link repo={source_to_link_against}"
        else:
            if self.ver == '':
                raise ValueError('when setting up a fresh run, version is required!')
            setup_command = f"{setup_exec} ver={self.ver} config={self.config} runid={self.runid} repo={self.repo}"

        # add additional flags if necessary
        setup_command = self._add_flags(setup_command)

        try:
            self.run_command(setup_command, setup_run_env=False, run_directory = os.path.dirname(self.run_directory))
        except RemoteError as e:
            # strict checking error can fail here on older versions of CanESM5 if the linked code has been changed
            if '*** STRICT CHECKING ERROR ***' in str(e):
                pass
            else:
                raise e

    def _setup_canesm_cfg(self, update_rtdiag: bool = True):

        self.canesm_cfg['start_time'] = f'{self.start_year}:{self.start_month}'
        self.canesm_cfg['stop_time'] = f'{self.stop_year}:{self.stop_month}'
        if update_rtdiag:
            self.canesm_cfg['start_rtdiag'] = f'{self.start_year}:{self.start_month}'
        self._process_file('canesm.cfg', self.canesm_cfg)

    def _setup_namelist(self, filename, settings):
        try:
            self._process_file(filename, settings, src_dir = self.namelist_directory)
        except IOError:
            raise IOError(f'Namelist modification failed! {filename} doesnt exist!')

    def _setup_namelists(self):
        """
            Execute desired namelist modifications
        """
        if not self.namelist_mods:
            return

        for namelist, modifications in self.namelist_mods.items():
            self._setup_namelist(namelist, modifications)

    def _sync_runpath_file(self, filename, settings):

        if not settings:
            return

        src_dir = self.runpath
        result = self.run_command('ls $RUNPATH')
        files = [f for f in result.stdout.split('\n') if filename.lower() in f.lower()]
        for file in files:
            self.logger.info(self.runid + ': updating ' + filename + ' in $RUNPATH/' + file)
            self.run_command('chmod u+w $RUNPATH/' + file)
            self._process_file(file, settings, src_dir=src_dir)
            self.run_command('chmod u-w $RUNPATH/' + file)

    def _setup_cpp_defs(self):
        """
        Process the compiler definitions.
        """
        if not self.cpp_defs:
            return
        else:
            raise NotImplementedError("Cpp rangling hasn't been added yet!")

        #src_dir = os.path.join(self.run_directory, 'cppdir_' + self.runid)
        #for file in self.cpp_defs.keys():
        #    self._process_file(file, self.cpp_defs[file], src_dir=src_dir, processor=ProcessCPPDef)

    def _save_restart_files(self):
        """
        Move restart files from disc or tape to $RUNPATH
        """
        if is_null(self.tapeload):
            return

        self.logger.info(f'{self.runid} : saving restarts...')

        # if desired, add options to pull namelists from restarts
        option_str = ''
        opt_flag_dict = { "agcm" : "-a", "coupler" : "-c", "ocean" : "-n" }
        if self.namelists_to_pull_from_restarts:
            for component, component_namelists in self.namelists_to_pull_from_restarts.items():
                try:
                    opt_flag = opt_flag_dict[component]
                    for namelist in component_namelists:
                        option_str += f' {opt_flag} {namelist}'
                except KeyError:
                    err_string = "namelist_to_pull_from_restarts expects the top level keys to be agcm/coupler/ocean!"
                    raise KeyError("err_string")

        if self.tapeload:
            call_script = 'tapeload_rs'
        else:
            call_script = 'save_restart_files'

        command = f'{call_script} {option_str}'
        print(command)
        self.run_command(command)

    def _config_job(self):
        """
            Run the configuration tool
        """
        config_tool = "config-canesm"
        self.run_command(f'{config_tool}')

    def _add_flags(self, setup_command):
        """
        Add additional parameters to the setup-canesm script call

        Parameters
        ----------
        setup_command :
            setup-canesm string that the flags will be added to
        """
        if self.setup_flags is not None:
            setup_command += ' ' + self.setup_flags
        return setup_command

    def _compile(self, compile_opts: str = ''):
        """
        Compile the code
        """
        compile_command=f'compile-canesm.sh {compile_opts}'
        self.logger.info(f'{self.runid} : compiling...')
        self.run_command(compile_command)

    def _link_exec_to_base_run(self, base_directory : str,
                                exec_storage_dir_variable : str = "EXEC_STORAGE_DIR",
                                shell_config_var_file : str = "config/canesm-shell-params.sh"):
        """
            Instead of compiling, create a link to the executable
            storage directory of the base run and setup the local
            $WRK_DIR/executables soft link

            Notes:
                - run_command runs the remote command in the run directory,
                    which is why we don't need to construct a full path to the
                    current member's config var file
                - to stop annoying additional output from the grep command, we
                  need to make a null start_up_command and force it to avoid
                  setting up the run environment
        """
        # construct path to base member config file
        base_member_shell_config_var_file = os.path.join(base_directory,shell_config_var_file)

        # first extract the storage directory from the shell params file
        grep_command = f"grep -Po --color=never '(?<={exec_storage_dir_variable}\=).*'"
        base_member_exec_storage_dir    = self.run_command(f"{grep_command} {base_member_shell_config_var_file}",
                                                            start_up_command = "",
                                                            setup_run_env = False,
                                                            hide = True).stdout.strip()
        current_member_exec_storage_dir = self.run_command(f"{grep_command} {shell_config_var_file}",
                                                            start_up_command = "",
                                                            setup_run_env = False,
                                                            hide = True).stdout.strip()

        # create a soft link between the current member's storage dir and that from the base dir
        self.run_command(f"ln -s {base_member_exec_storage_dir} {current_member_exec_storage_dir}")

        # create a soft link in the current member's $WRK_DIR to the exec storage dir
        exec_dir_name = os.path.basename(current_member_exec_storage_dir)
        self.run_command(f"ln -s {current_member_exec_storage_dir} $WRK_DIR/{exec_dir_name}")

    def _copy_file(self, src_file : str, dest_file: str, link : bool = True):
        if link:
            self.run_command(f'ln -s {src_file} {dest_file}')
        else:
            self.run_command(f'cp {src_file} {dest_file}')

    def _setup_readme(self):
        """
        Update the README.md file for the run with the setup parameters
        """

        readme = (
            f'# CanESM Run: {self.runid}\n'
            f'## Description\n'
            f'This run was generated using the setup-ensemble script\n\n'
            f'start time: {self.start_time}\n'
            f'stop time: {self.stop_time}\n\n'
            f'### Setup code information\n'
            f'code repository: https: // gitlab.science.gc.ca / CanESM / canesm-ensemble\n'
            f'code version: {canesm.__version__}\n\n'
            f'### Restart information\n'
            + "".join([f'{key}:{val}\n' for key, val in self.restart_files.items()])
            + f'\n### namelist changes \n'
            + "".join([f'{namelist}\t{key}:{val}\n' for namelist, mods in self.namelist_mods.items() 
                        for key, val in mods.items()])
            + f'\n### canesm.cfg changes \n'
            + "".join([f'{key}:{val}\n' for key, val in self.canesm_cfg.items()])
            + f'\n### cpp definition changes \n'
            + "".join([f'{nkey}\t{key}:{val}\n' for nkey in self.cpp_defs
                       for key, val in self.cpp_defs[nkey].items()])
            + f'\n\n## List of Changes/Interventions\n'
        )

        file = 'README.md'
        with RemoteFile(os.path.join(self.run_directory, file),
                        self.machine, self.user, mode='w') as f:
            f.write(readme)

    @wrap_canesm_remotefail
    def run_command(self, command: str, setup_run_env: bool = True, run_directory: str = None, hide: bool = False,
                        start_up_command: str = 'source ~/.profile > /dev/null 2>&1;'):
        """
        runs a command on the remote maching from the :py:attr:`run_directory`

        Parameters
        ----------
        command :
            command that will be ran on the remote machine
        setup_run_env :
            if True commands are run inside the job environment
        run_directory :
            location to run the command from. By default commands are ran from the `run_directory`
        hide : 
            if True output from remote command is supressed
        start_up_command:
            command to run at login to the remote machine - defaults to 'source ~/.profile'
        """
        if run_directory is None:
            run_directory = self.run_directory

        # construction command
        if setup_run_env:
            command = f'{start_up_command} . env_setup_file && {command}'
        else:
            command = f'{start_up_command} {command}'

        with Connection(self.machine, user=self.user) as c:
            with c.cd(run_directory):
                result = c.run(command, watchers=self.responders, hide=hide)
        return result

    def get_setup_canesm_exec(self):
        """
        Return full path to the setup-canesm executable.

        """
        setup_exec_name     = "setup-canesm"
        user_exec           = None
        remote_exec_search_output = self.run_command(f'which {setup_exec_name} || :', # avoid non-zero exit status
                                        setup_run_env=False,
                                        run_directory="$HOME").stdout
        for line in remote_exec_search_output.split('\n'):
            if re.match(f'\S*/{setup_exec_name}', line):
                user_exec = line

        if user_exec:
            returned_exec = user_exec
        else:
            message = f"{setup_exec_name} not found on your PATH variable! Is your .profile setup?"
            raise FileNotFoundError(message)
        return returned_exec

    def setup_job(self, share_source_code: bool = False, share_executables: bool = False,
                    base_member: bool = False, base_directory: str = None):
        """
        This method performs five main tasks
           - clone the code into the run directory (or copy from a basejob)
           - set changes in canesm.cfg
           - configure the model's/sequencer's downstream files
           - make additional modifications to namelists as specified by the user
           - compile the code
           - save restart files

        This should be called after all of the settings are configured.

        Parameters
        ----------
        share_source_code : optional
            If true, indicates that a single source directory will be used by multiple runs
        share_executables : optional
            If true, executables will be used from the base_directory run.
        base_member : optional
            If true, indicates that this is this base setup for an ensemble
        base_directory : optional
            Optional run directory of the base job that the code will be shared from. 
            Must be provided if share_source_code is true and this setup isn't the base
            member

        """

        self.logger.info(f'{self.runid}: setting up {self.runid}...')
        if share_source_code and not base_member:
            if base_directory is None:
                raise ValueError('Source code sharing requested but no base_directory given!')
            else:
                # need to add $HOME to this so we can reference the path to the source
                #   in the remote commands, which assume their ran in self.run_directory
                #   (which is assumed to be a relative path from HOME... [PS: this assumption
                #   should be refactored out])
                path_to_base_run = os.path.join("$HOME", base_directory)

        # determine how the run will be setup
        if share_source_code:
            if base_member:
                clean_setup = True
            else:
                clean_setup = False
        else:
            clean_setup = True

        # clone the code
        if clean_setup: 
            self._run_setup_command()
        else: 
            base_run_source = os.path.join(path_to_base_run,"CanESM_source_link")
            self._run_setup_command(link_source=True, source_to_link_against=base_run_source)

        # save the readme file
        #self._setup_readme()

        # setup config files
        #   - namelists are done after restarts because we might pull namelists from restart files
        self._setup_canesm_cfg(update_rtdiag=False)
        #self._setup_cpp_defs()

        # configure the job's downstream files
        self._config_job()

        # determine if we need to compile the code and if any options are necessary
        compile_opts = ""
        if share_source_code:
            if share_executables:
                if base_member:
                    need_compilation = True
                else:
                    need_compilation = False
                    output = f'{self.runid} : using executables from {base_directory} instead of compiling...'
                    self.logger.info(output)
                    print(output)
            else:
                need_compilation = True
                # compile in "local" temporary directory to keep isolated build directories
                compile_opts += " -l"
        else:
            need_compilation = True

        if need_compilation:
            self._compile(compile_opts = compile_opts)
        else:
            self._link_exec_to_base_run(base_directory = path_to_base_run)

        # save restart files to backend
        self._save_restart_files()

        # perform explicit namelist updates
        self._setup_namelists()

        self.is_setup = True
        self.logger.info(f'{self.runid} : setup succesful for {self.runid}')

    def delete_job(self):
        """
        Delete all the files on the remote machine related to this job. This includes the working directory,
        source code and restart files.
        """
        try:
            runpath = os.path.split(self.runpath)[0]  # drop /data
            if self.runid not in runpath:  # make sure we know what we are deleting
                raise ValueError(self.runid + ' was not found in ' + runpath + ', not deleting this path')

            self.run_command('chmod -R u+w ' + runpath)
            self.run_command('rm -rf ' + runpath)
        except RemoteError as e:
            self.logger.debug(self.runid + ': ' + str(e))

        try:
            ccrnsrc = os.path.split(self.ccrnsrc)[0]  # drop /code
            if self.runid not in ccrnsrc:  # make sure we know what we are deleting
                raise ValueError(self.runid + ' was not found in ' + ccrnsrc + ', not deleting this path')

            self.run_command('rm -rf ' + ccrnsrc)
        except RemoteError as e:
            self.logger.debug(self.runid + ': ' + str(e))

        try:
            wrk_dir = self.wrk_dir
            if self.runid not in wrk_dir:  # make sure we know what we are deleting
                raise ValueError(self.runid + ' was not found in ' + wrk_dir + ', not deleting this path')

            self.run_command('rm -rf ' + wrk_dir, setup_run_env=False)
        except RemoteError as e:
            self.logger.debug(self.runid + ': ' + str(e))

    def extend_run(self, years: Union[int, str]):
        """
        Extend the job to a longer simulation time

        Parameters
        ----------
        years :
            Extend the run by 'YYYY_mMM' time. If an integer is provided the run will be extended by this number of
            years. For example, to extend the run by 2 years and 6 months use format years='2_m06'.
        """

        self.start_time = add_time(f'{self.stop_year}_m{self.stop_month}', '0000_m01')
        self.stop_time = add_time(f'{self.stop_year}_m{self.stop_month}', years)
        self._setup_canesm_cfg(update_rtdiag=False)
        self._config_job()

    def submit_job(self):
        """
        submits the job to backend.

        Raises
        ------
        ValueError
            If the model has not been setup
        """
        if not self.is_setup:
            raise ValueError('the job must be setup before submitting')
        launch_command = f"expbegin -e {os.environ['SEQ_EXP_HOME']} -d {datetime.now().strftime('%Y%m%d%H')}"
        self.run_command(launch_command)
        print(f"submitted {self.runid}")
