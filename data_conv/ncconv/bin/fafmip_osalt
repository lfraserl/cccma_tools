#!/bin/ksh -e
#
# 1. calculate Ocean Model Cell Thickness - use missing values over land
#    grido gz BETA DA DX DY DZ, BETA*DZ,
# 2. calculate thkcello*1025.0*(inp1+inp2+inp3)
# 3. for chunk size bigger than 6 years, line (cat thkcello .... > thkc) needs to be adjusted.
#
# Run example: fafmip_osalt sc_dpd-002_4501_4505_gz data_description sadx sady sadz out
# Run example: fafmip_osalt sc_dpd-002_4501_4505_gz data_description dels out

if [[ $# -lt 4 || $# -gt 7 ]] ; then
  echo "Error in $0: invalid number of arguments $#"
  exit 1
fi

pfx=$1
desc=$2
release desc inp1 inp2 inp3
access  desc ${pfx}_${desc}

if [ $# -eq 4 ] ; then
  inp1=$3
  out=$4

  access  inp1 ${pfx}_${inp1}
  cp inp1 isum
elif [ $# -eq 5 ] ; then
  inp1=$3
  inp2=$4
  out=$5

  access  inp1 ${pfx}_${inp1}
  access  inp2 ${pfx}_${inp2}
  add inp1 inp2 isum
elif [ $# -eq 6 ] ; then
  inp1=$3
  inp2=$4
  inp3=$5
  out=$6

  access  inp1 ${pfx}_${inp1}
  access  inp2 ${pfx}_${inp2}
  access  inp3 ${pfx}_${inp3}
  add inp1 inp2 itmp
  add inp3 itmp isum
fi
release ${pfx}_${out}

grido desc beta da dx dy dz
$RTEXTBLS/lopgm_o4xa/mlt dz beta thk
echo "C*FMSKPLT         -1 NEXT   GT      0.00    1     1.E38    1" | ccc fmskplt thk thkcello
release desc beta da dx dy dz thk

# remove superlable (note that timestamp of superlable is 0)
echo "C*SELECT.  STEP         1 999999999    1     -900099999 NAME  ALL"|ccc select isum itmp

if [[ $inp1 == dels ]]; then
  echo "C*TSTEP                                YYYYMM           YYYY"| ccc tstep itmp tend
  echo "C*XLIN     31536000.      0.E0         1"|ccc xlin tend iann
else
  # get annual mean from monthly mean
  printf "C* BINSML    12    0    0   12                                        \n"  > ic.binsml
  printf "C            31   28   31   30   31   30   31   31   30   31   30   31\n" >> ic.binsml
  $RTEXTBLS/lopgm_o4xa/binsml itmp iann input=ic.binsml
fi

cat thkcello thkcello thkcello thkcello thkcello thkcello > thkc
/users/tor/acrn/rsk/bin_aix/mlt_spv iann thkc thko
echo "C*XLIN        1025.0"|ccc /users/tor/acrn/rsk/bin_aix/xlin_spv thko ${pfx}_${out}

release inp1 inp2 inp3 itmp isum tend iann thko thkcello thkc ic.binsml

