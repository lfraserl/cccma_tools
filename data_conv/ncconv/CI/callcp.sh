#!/bin/bash
# This is a simple shell script which calls the pycmor interface with some basic options.
# For more options and info see python pycmor.py -h
#
# NCS 12/2018

# make sure path_export.sh has been sourced
bail(){
echo " *** ERROR *** $1"
exit 1
}
[ -z "$NCCONV_DIR" ] && bail 'variable NCCONV not set.. be sure to source path_export.sh'
[ -z "$HALLN" ] && bail 'variable HALLN not set.. be sure to source path_export.sh'
which pycmor.py > /dev/null 2>&1 || bail 'pycmor.py not on your $PATH variable... source path_export.sh and try again'

# define conversion vars
runid='p2-his01'                            # The runid of the run being converted
nemo_mask='nemo_mesh_mask_rc3.nc'           # Nemo Mask file to use for this conversion
user='cpd103'                               # The user who did the run (not necessarily the same as the user doing the netcdf conversion)
vtabdir=$NCCONV_DIR/tables/variable_tables  # Name of directory containing the variable tables 
chunk='2005_2005'                           # Time chunk to process
outpath='.'                                 # output location of netcdf files produced by CMOR
version='continuous-integration'

# Set up additional environment considerations
source /home/scrd101/generic/sc_cccma_setup_profile     # necessary for many optional decks
export DATAPATH_DB=/space/hall${HALLN}/sitestore/eccc/crd/ccrn/users/$user/$runid/datapath_local.db:/space/hall${HALLN}/sitestore/eccc/crd/ppp_data/database_dir/datapath_ppp${HALLN}.db    # update DATAPATH_DB to point to the database for the desired run

cmor_table='Amon'
pycmor.py -L -m $nemo_mask -r $runid -t $cmor_table -d $vtabdir -k $chunk -o $outpath -v $version -c 'tas pr'

cmor_table='Omon'
pycmor.py -L -m $nemo_mask -r $runid -t $cmor_table -d $vtabdir -k $chunk -o $outpath -v $version -c 'thetao uo vo obvfsq tos'
