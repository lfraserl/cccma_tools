#!/usr/bin/python/
'''
Module to compute elapsed times and dates for labelling CCCma model output.

Allowed CF-compliant calendar types are listed in:
    http://cfconventions.org/Data/cf-conventions/cf-conventions-1.7/cf-conventions.html#calendar

JA, Jan 2019
'''

import numpy as np
import calendar as cal

use_cdtime = not True
use_cftime = True

if use_cdtime:
    import cdtime   
    #print cdtime.__file__
    method_cdtime = 2
if use_cftime:
    import cftime

assert not (use_cftime and use_cdtime), 'pick one'

# Define constants that set how many seconds per day, days per month, etc.
conversion_factors = {
    'hours per day'         : 24.
,   'minutes per hour'      : 60.
,   'seconds per minute'    : 60.
}
conversion_factors.update({
    'minutes per day'       : conversion_factors['minutes per hour']*conversion_factors['hours per day']
,   'seconds per hour'      : conversion_factors['seconds per minute']*conversion_factors['minutes per hour']
})
conversion_factors.update({
    'seconds per day'       : conversion_factors['seconds per hour']*conversion_factors['hours per day']
})


date_order = ['year', 'month', 'day', 'hour', 'minute', 'second']

valid_calendars = ['gregorian', 'standard', 'proleptic_gregorian', 'julian', 
                   'noleap', '365_day', 
                   'all_leap', '366_day',
                   '360_day',
                   'none'
                   ]

if use_cdtime:
    # Map calendar names in valid_calendars to corresponding cases handled by the cdtime module.
    # https://cdat.llnl.gov/documentation/cdms/cdms_3.html
    d_cdtime = {
        'standard'              : cdtime.MixedCalendar  # there's also cdtime.StandardCalendar, what is that?
    ,   'gregorian'             : cdtime.GregorianCalendar
    ,   'proleptic_gregorian'   : cdtime.GregorianCalendar
    ,   'julian'                : cdtime.JulianCalendar

    ,   '365_day'               : cdtime.NoLeapCalendar
    ,   'noleap'                : cdtime.NoLeapCalendar
    
    ,   '360_day'               : cdtime.Calendar360
    }
    valid_calendars = [s for s in valid_calendars if s in d_cdtime]

def calendar_check(calendar):
    assert isinstance(calendar, str), 'Calendar should be string'
    # check that calendar is one of the valid types
    assert calendar in valid_calendars, 'Unknown calendar type: ' + calendar
    
def has_leap_years(calendar):
    '''
    Return True if the calendar type has leap years
    '''
    calendar_check(calendar)
    return calendar in ['gregorian', 'standard', 'proleptic_gregorian', 'julian']

def is_leap_year(y, calendar):
    '''
    Return True if year 'y' is a leap year, otherwise False.
    '''
    calendar_check(calendar)
    y = int(y)
    if      calendar in ['gregorian', 'standard', 'proleptic_gregorian']:
        if True:
            return cal.isleap(y)
            '''
            The python calendar module ('cal' here) assumes the Gregorian calendar. 
            From https://docs.python.org/2/library/calendar.html, accessed 8 Jan 2019:

                Most of these functions and classes rely on the datetime module which uses an idealized calendar, 
                the current Gregorian calendar indefinitely extended in both directions. This matches the definition 
                of the "proleptic Gregorian" calendar in Dershowitz and Reingold's book "Calendrical Calculations", 
                where it's the base calendar for all computations.
            '''
        else:
            # Probably better to use calendar.isleap() instead. Although its results seem to agree with this code. 
            # But better to rely on a standard function vetted by many users.
            
            if      np.mod(y,400) == 0: return True
            elif    np.mod(y,100) == 0: return False
            elif    np.mod(y,4) == 0:   return True
            else:   return False

    elif    calendar in ['julian']:
        '''
        Julian calendar is like Gregorian except in the definition of leap years. 
        https://en.wikipedia.org/wiki/Julian_calendar, accessed 8 Jan 2019:
        
            The Gregorian calendar has the same months and month lengths as the Julian calendar, but, 
            in the Gregorian calendar, year numbers evenly divisible by 100 are not leap years, except 
            that those evenly divisible by 400 remain leap years.
            
        Also useful:
            https://www.tondering.dk/claus/cal/julian.php    
            
        Google reveals some python modules that could be useful (although I haven't used them yet):
            https://pypi.org/project/jdcal/
                convert between Gregorian calendar dates and julian days
                seems to be bundled with anaconda
            https://pypi.org/project/convertdate/
                convert between Gregorian dates and other calendar systems
        '''
        assert y > 5    
        # prior to year 8, leap years are every 3 years, except for a gap. 
        # could allow them here, but would it ever be needed?
        # See https://www.tondering.dk/claus/cal/julian.php

        if      np.mod(y,4) == 0:   return True
        else:   return False

    else:
        return False

def get_dpm(calendar):
    '''
    Return array giving number of days per month for a given calendar type.
    Note, this does NOT return the days per month in a leap year. It doesn't take the year as input.
    (Leap years are handled elsewhere in this module.)
    '''
    calendar_check(calendar)
    if      calendar in ['gregorian', 'standard', 'proleptic_gregorian', 'julian',
                         'noleap', '365_day']:
        dpm = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
        
    elif    calendar in ['all_leap', '366_day']:
        dpm = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

    elif    calendar in ['360_day']:
        dpm = 30*np.ones(12)
        
    elif    calendar in ['none']:
        # Appropriate for model runs that simulate a fixed time of year.
        # (See http://cfconventions.org/Data/cf-conventions/cf-conventions-1.7/cf-conventions.html#calendar)
        # Not sure what value of dpm would be useful here, if any.
        # Specifying dpm = [30], say, means that each "year" has 30 days and "month" is always 1. 
        # Specifying dpm = 30*np.ones( some big enough number ) means that each "month" has 30 days and "year" is always 1. 
        dpm = [30]
        #dpm = 30*np.ones(1000)

    else:
        assert False, 'Unknown calendar type: ' + calendar

    return np.array(dpm)

def get_dpy(calendar, year=None):
    '''
    Return number of days per year.
    '''
    calendar_check(calendar)
    dpy = sum( get_dpm(calendar) )
    if has_leap_years(calendar):
        assert year is not None, 'Year is required for calulating days per year for calendar: ' + calendar
        if is_leap_year(year, calendar):
            dpy += 1
    return dpy
    
def get_dpmy(y0, y1, calendar):
    '''
    Return year x month matrix giving days per month for each month. 
    (row = year, column = month)
    
    If the gap between y0 & y1 is large, the matrix dpmy could get big and the function
    could slow down. This could slow things down when dealing with dates that are a long
    way from a reference date, e.g. y1 - y0 ~ 1000 or more. Most model runs are not this
    long, so with appropriate choice of reference dates we should be able to ignore this 
    problem. If it's necessary to fix it, one possibility is to generate dpmy with only 
    a few rows, including years y0 and y1, and supply a separate value giving the count 
    of days for all the intervening years. Those values could be pre-computed for specific 
    year ranges and supplied in a lookup table.
    '''
    dpm = get_dpm(calendar)
    y0, y1 = np.sort([y0,y1]) # ensure y0 = min value, y1 = max value
    years = np.arange(y0, y1+1) # years (the actual year numbers, not indices)
    y = np.ones(len(years))
    y.shape = (len(y),1)
    nm = len(dpm)
    dpm.shape = (1,nm)
    dpmy = y*dpm
    #print dpmy.shape, leap_years
    ind = [m for m,y in enumerate(years) if is_leap_year(y, calendar)]
    #print years, ind
    for m in ind: dpmy[m][1] += 1 # change Feb from 28 to 29 days
    months = np.arange(nm) + 1  # month numbers (1 = Jan, 2 = Feb, ...)
    a = np.ones(dpmy.shape)
    years.shape  = (len(years), 1)
    months.shape = (1, len(months))
    years = years*a
    months = months*a
    assert years.shape == dpmy.shape
    assert months.shape == dpmy.shape
    return dpmy, years, months

def date_check(d):
    assert isinstance(d, dict)
    # df = default values for date dicts, and defines the allowed time fields. 
    df = {'year' : 0, 'month' : 1, 'day' : 1, 'hour' : 0, 'minute' : 0, 'second' : 0}
    no_default = ['year', 'month'] # require these inputs be defined by the user (i.e. no default is allowed)
    for s in d: # loop over time fields passed into the function (in 'd')
        assert s in df, 'Unknown time field: ' + s
    for s in df: # loop over allowed time fields
        if s not in d: # if an allowed time field is not found in the input dict 'd'
            if s in no_default:
                assert False, 'User must provide time field: ' + s
            else:
                d[s] = df[s] # assign the time field its default value

def date_print(d, return_str=False):
    '''
    Pretty print of date dict.
    '''
    date_check(d)
    #d_fmt = {s : '%g' for s in date_order}
    d_fmt = {s : '%.2i' for s in date_order}
    d_fmt['year'] = '%5.4i' # extra space to allow for possible negative sign
    l = [str(str('%s : ' + d_fmt[s]) % (s, d[s])) for s in date_order]
    w = ', '.join(l)
    if return_str:
        return w
    else:
        print w

def date2time(d, d_ref, calendar, t_units='days'):
    '''
    Return elapsed time (i.e. a monotonic time coordinate) at a given date since a reference date. 
    Input dict 'd' is the given date, and input dict 'd_ref' is the reference date.
    t_units sets the units of the output time.
    
    Format of the date dicts, 'd' and 'd_ref', is
        {'year' : 0, 'month' : 1, 'day' : 1, 'hour' : 0, 'minute' : 0, 'second' : 0}
    Note that:
    - hour-minute-second follows the usual timekeeping convention: hour=0 refers to midnight (i.e. 12am), 
      and minute=0 is the first minute of an hour, e.g. 1:00am. 
    - month,day follow the usual convention that first values in a year are 1,1 (rather than 0,0). So
      day=1 of a given year, NOT day=0, gives time zero in that year.
    
    Handles the calendar types listed in valid_calendars.
    '''
    calendar_check(calendar)

    date_check(d)
    date_check(d_ref)

    d_tar = dict(d)
    d_ref = dict(d_ref)
    
    if use_cftime:
        t_tar = tuple([ d_tar[s] for s in date_order ])
        dt_tar = cftime.datetime(*t_tar)
        t_ref = tuple([ d_ref[s] for s in date_order ])
        dt_ref = cftime.datetime(*t_ref)
        ref_date_str = t_units + ' since ' + str(dt_ref)
        t_day = cftime.date2num(dt_tar, ref_date_str, calendar=calendar)
    
    elif use_cdtime:
        for d in [d_tar, d_ref]:
            for s in date_order[:-1]: 
                d[s] = int(d[s])

        if method_cdtime == 1:

            cdtime.DefaultCalendar = d_cdtime[ calendar ] # this must be set for cdtime to produce the correct results for the given calendar
            
            #print ' '*100 + '--->', cdtime.DefaultCalendar, d_cdtime
            
            tc_tar = cdtime.comptime( *tuple([ d_tar[s] for s in date_order ]) )
            tr_tar = tc_tar.torel(t_units)

            tc_ref = cdtime.comptime( *tuple([ d_ref[s] for s in date_order ]) )
            tr_ref = tc_ref.torel(t_units)
            
            t_day = tr_tar.value - tr_ref.value
            
            #print tc_tar, tc_ref
            #print tr_tar, tr_ref
    
        elif method_cdtime == 2:

            tc_tar = cdtime.comptime( *tuple([ d_tar[s] for s in date_order ]) )
            tc_ref = cdtime.comptime( *tuple([ d_ref[s] for s in date_order ]) )
            ref_date_str = t_units + ' since ' + str(tc_ref)
            tr_tar = cdtime.c2r(tc_tar, ref_date_str, d_cdtime[calendar])
            '''
            In the latest CMOR env, CMOR ver 3.4, the above call to cdtime.c2r is invalid. 
            (In the CMOR env for CMOR ver 3.3 it works fine.)
            According to the docstring the function should take an optional argument specifying the calendar,
            but instead we get:
                TypeError: c2r() takes exactly 2 arguments (3 given)
            Having tried method_cdtime = 1 and 2, neither seems to work properly in for the ver 3.4 CMOR env.
            So use cftime instead. (16jan.19)
            '''
            t_day = tr_tar.value
            
    else:
    
        dpmy, years, months = get_dpmy(d_ref['year'], d_tar['year'], calendar)

        if not True:
            print dpmy
            print years
            print months
        
        dpmy   = dpmy.flatten()
        years  = years.flatten()
        months = months.flatten()

        te = np.array( [ dpmy[:m].sum() for m in range(len(dpmy)) ] ) # time elapsed prior to start of month
        assert te.shape == dpmy.shape
        
        hpd = conversion_factors['hours per day']
        mpd = conversion_factors['minutes per day']
        spd = conversion_factors['seconds per day']
        
        lt = []
        for d in [d_tar, d_ref]:
            ind = np.where( np.logical_and( years == d['year'], months == d['month'] ) )[0]
            assert len(ind) == 1
            m = ind[0]
            # te[m] = elapsed time (in days) at the start of the month for this date
            #print m, years[m], months[m],  date_print(d, return_str=True)
            t_day = te[m] + d['day'] - 1 + d['hour']/hpd + d['minute']/mpd + d['second']/spd
            lt += [t_day]
            
        t_day = lt[0] - lt[1] # time (in days) elapsed since the reference date
        
        
    if not True:
        print
        print '    calendar:', calendar
        print '    ref date:', date_print(d_ref, return_str=True)
        print ' target date:', date_print(d_tar, return_str=True)
        print 'elapsed time:', t_day

    if      t_units in ['days', 'd']: 
        return t_day
    elif    t_units in ['s']:   
        return t_day*spd
    else:   
        assert False, 'Unrecognized units: ' + t_units
    
def time2date(t, d_ref, calendar, t_units='days'):
    '''
    Return date dict giving the date at which input time 't' has elapsed since the reference date, 'd_ref'
    This is the inverse of date2time(). See that function for comments on the format of the date dict.
    t_units gives the units of the input time 't'.
    Handles the calendar types listed in valid_calendars.
    '''
    assert isinstance(t, (int, float))
    calendar_check(calendar)

    spd = conversion_factors['seconds per day']
    
    if      t_units in ['days', 'd']: 
        t_day = t
    elif    t_units in ['s']:   
        t_day = t/spd    # convert seconds to days
    else:   
        assert False, 'Unrecognized units: ' + t_units
    del t
    
    date_check(d_ref)
 
    if use_cftime:
      
        t_ref = tuple([ d_ref[s] for s in date_order ])
        dt_ref = cftime.datetime(*t_ref)
        ref_date_str = t_units + ' since ' + str(dt_ref)
        dt_tar = cftime.num2date(t_day, ref_date_str, calendar=calendar)
        
        d_tar = {s : getattr(dt_tar,s) for s in date_order}
    
    elif use_cdtime:
        
        for s in date_order[:-1]: 
            d_ref[s] = int(d_ref[s])    
            
        if method_cdtime == 1:
                
            cdtime.DefaultCalendar = d_cdtime[ calendar ] # this must be set for cdtime to produce the correct results for the given calendar
                
            tc_ref = cdtime.comptime( *tuple([ d_ref[s] for s in date_order ]) )
            tr_ref = tc_ref.torel(t_units)
            
            t_day += tr_ref.value
            
            tr_tar = cdtime.reltime(t_day, 'days')
            tc_tar = tr_tar.tocomp()
            
        elif method_cdtime == 2:
            
            tc_ref = cdtime.comptime( *tuple([ d_ref[s] for s in date_order ]) )
            ref_date_str = t_units + ' since ' + str(tc_ref)
            tr_tar = cdtime.reltime(t_day, ref_date_str)
            tc_tar = cdtime.r2c(tr_tar, d_cdtime[calendar])
            
        d_tar = {s : getattr(tc_tar,s) for s in date_order}


    else:

        d_tar = {}

        y0 = d_ref['year']
        max_dpy = sum( get_dpm(calendar) ) # max possible number of days in the calendar
        t_day = float(t_day)
        #y1 = int( np.ceil( t_day/max_dpy ) )
        y1 = int( np.sign(t_day)*np.ceil( abs(t_day)/max_dpy ) )
        
        #print t_day, max_dpy
        #print y0, y1

        dpmy, years, months = get_dpmy(d_ref['year'], d_ref['year'] + y1, calendar)
        
        if not True:
            print dpmy
            print years
            print months

        
        dpmy   = dpmy.flatten()
        years  = years.flatten()
        months = months.flatten()

        te = np.array( [ dpmy[:m].sum() for m in range(len(dpmy)) ] ) # time elapsed prior to start of month
        assert te.shape == dpmy.shape
        
        # Find location in 'te' of the reference date's year & month
        ind = np.where( np.logical_and( years == d_ref['year'], months == d_ref['month'] ) )[0]
        assert len(ind) == 1
        m = ind[0]
        
        hpd = conversion_factors['hours per day']
        mpd = conversion_factors['minutes per day']
        
        d = d_ref
        t_day_ref = te[m] + d['day'] - 1 + d['hour']/hpd + d['minute']/mpd + d['second']/spd

        #print te
        #print t_day + t_day_ref

        m = np.where( te <= t_day + t_day_ref )[0][-1]
        d_tar['day'], frac_day = np.divmod(t_day + t_day_ref - te[m] + 1, 1)
        
        d_tar['year'] = years[m]
        d_tar['month'] = months[m]

        if frac_day != 0:
            # compute hours, minutes, seconds
            sph = conversion_factors['seconds per hour']
            spm = conversion_factors['seconds per minute']
            
            ts = np.round(frac_day*spd) # integer number of seconds
            d_tar['hour'], ts   = np.divmod( ts, sph )
            d_tar['minute'], ts = np.divmod( ts, spm )
            d_tar['second']     = ts


    if not True:
        print
        print '    calendar:', calendar
        print 'elapsed time:', t_day
        print '    ref date:', date_print(d_ref, return_str=True)
        print ' target date:', date_print(d_tar, return_str=True)
        
    date_check(d_tar)
   
    for s in date_order:
        if np.sign(d_tar[s]) == 0: d_tar[s] = np.abs(d_tar[s]) # this just stops 0 being displayed as -0.

    return d_tar


