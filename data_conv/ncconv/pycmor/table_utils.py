""" 
    This module does most of the heavy lifting during the "CMORization" 
    process, with the main function being ```convert_table()``. The 
    rest of the functions are used for things like file system manipulation,
    running optional decks, or parsing that data contained in the 
    CCCma variable tables.

    .. note::

        - a code review is required for this module.. its getting a bit hairy
"""
import sys
import sqlite3  
import resource
import copy
import json
import re
import os
import glob
import nmor
import amor
import shutil
import traceback
import subprocess
import getpass
import datetime
import cftime
import itertools
import xarray as xr
import cmor
import compliance_checks as cc
import answer_checks as ac
import pandas as pd
import pycmor_to_deprecate
import multiprocessing

# specific deployment specific locations
_source_location_template = "/home/ords/crd/ccrn/{user}/{runid}/code/CanESM"        # gets overwritten if CCRNSRC is populated
_run_event_database_template = "/home/ords/crd/ccrn/{user}/{runid}/{runid}-log.db"  # gets overwritten if CCRNSRC is populated
_default_runlog_dir = '/home/scrd104/public_html/canesm-run-logs' 

user_req_vars_in_tables = []    # list of user requested vars that were found in requested CMOR tables
var_hashes = {}                 # dictionary to store the new variable hashes, if produced

def get_vartab(vtabdir, cmor_table):
    """Load the desired variable json table.

        Parameters
        ----------
            vtabdir : str
                absolute path of directory containing the variable json tables
            cmor_table : str
                cmor table identifier

        Returns
        -------
            table : dict
                dictionary containing the desired variable table data
    """
    # get table
    table_fl    = "CMIP6_{}.json".format(cmor_table)
    table_pth   = os.path.join(vtabdir,table_fl)
    with open(table_pth) as jsf:
        table = json.load(jsf)
        print('Loaded json table: {}'.format(table_pth))
    return table

def mkclean_dir(dirpth):
    """Build clean directory, removing old one if found.

        Parameters
        ----------
            dirpath : str
                directory to clean and create
    """
    # clean
    if os.path.isdir(dirpth):
        fls = os.listdir(dirpth)
        for f in fls: os.remove(os.path.join(dirpth,f))
        os.rmdir(dirpth)

    # make new dir
    os.makedirs(dirpth)

def convert_table(runid, cmor_table, vtabdir, chunk, model_hash="Unknown", cmorvars=None, nemo_mesh_mask_file=None,
                    use_cmorvar_logfile=False, check_deltas=False, test_compliance=False, production=False, outpath='output', 
                    no_exclusions=False, default_version='version', new_version='updated_version',
                    retracted_basefile='cmip6_experiments/retracted_vars.json',retracted_addfiles=None,
                    exptable='cmip6_experiments/cmip6_experiment.txt'):
    """
    Loads the desired CCCCma variable table and loops over the variables, converting 
    them into "CMORized" netcdf files.

    Performs considerable consitency checks, and meta-data manipulation prior to 
    converting through the `CMOR` software. During this process, ``convert_table``
    logs all errors or problems it sees along the way, writing high-level logging
    information to ``pycmor-driver-log_${runid}_${cmor_table}.log``, and low level,
    more detailed information to individual log files (if needed) in 
    ``./pycmor_logs/${cmor_table}/``. Output specific to ``CMOR`` is controlled by
    the value of ``use_cmorvar_logfile`` - see the description below for more info.

    Parameters
    ----------
        runid : str
            the runid being converted
        cmor_table : str
            the cmor table to process
        vtabdir : str
            path to directory containing the variable tables
        chunk : str
            time chunk identifier - accepts ``yyyy_yyyy`` or ``yyyymm_yyyymm`` format 
        model_hash : str **optional**
            source code hash for the runid in question. Defaults to "Unknown"
        cmorvars : list **optional**
            list of cmor variable names to process. Defaults to ``None``.
        nemo_mesh_mask_file : str **optional**
            The name of a saved nemo mesh mask file to use. Defaults to
            ``sc_{runid}_{chunk}_mesh_mask.nc``.
        use_cmorvar_logfile : bool **optional**
            if set to ``True``, stores individual ``CMOR`` logs in
            ``./cmor_logs/${cmor_table}/`` directory, else the ``CMOR`` output
            is sent to ``stdout/stderr``. Defaults to ``False``
        check_deltas : bool **optional**
            if set to ``True``, compare the variable-array hashes to those stored
            in ``bin/pycmor/continuous_integration``. Defaults to ``False``.
        test_compliance : bool **optional**
            if set to ``True``, run output files through predicted compliance pipeline.
            Defaults to ``False``. **This significantly increases runtime**.
        production : bool **optional**
            if set to ``True``, runs conversion in 'production' mode. As of
            2019-03-04, this only controls the presence of a Disclaimer message
            in the meta data. Defaults to ``False``.
        outpath : str **optional**
            path to where CMOR will write the output. Defaults to ``output``.
        no_exclusions : bool **optional**
            if set to ``True``, attempt to convert all possible variables,
            regardless of experiment/runid. Defaults to ``False``.
        default_version : str **optional**
            the default "version string" associated with the "CMORized" data, 
            which will be assigned to all converted data, unless this data 
            was found to be retracted. If this is the case, the value of ``new_version`` 
            is used. Defaults to ``'version'``.
        new_version : str **optional**
            the "version string" that will be attached to any variables that have 
            been found to be retracted, and a new version is being produced.
            Defaults to ``'updated_version'``.
        retracted_basefile : str **optional**
            the main file that is queried to ascertain if said variable was 
            retracted and needs an updated "version string".
            Defaults to ``'cmip6_experiments/retracted_vars.json'``.
        retracted_addfiles : list of str **optional**
            a list of ESGF logfiles that can also be queried for additional
            retraction information. Defaults to ``None``.
        exptable : str **optional**
            text file containing experiment information. Defaults to
            ``'cmip6_experiments/cmip6_experiment.txt'``.
    """

    #=================
    # General Preamble
    #=================
    #-- define log dirs and files
    pycmor_logfile  = 'pycmor-driver-log_{}_{}.log'.format(runid,cmor_table)
    pycmor_logfile  = open(pycmor_logfile,"w")
    pycmorlog_dir   = 'pycmor_logs'                                     # directory containing pycmor logs
    cmorlog_dir     = 'cmor_logs'                                       # directory containing cmor logs
    complog_dir     = 'compliance_logs'                                 # directory containing compliance logs
    pycmorlogtab_dir    = os.path.join(pycmorlog_dir,runid,cmor_table)  # runid/table specific pycmor logs
    cmorlogtab_dir      = os.path.join(cmorlog_dir,runid,cmor_table)    # runid/table specific cmor logs
    complogtab_dir      = os.path.join(complog_dir,runid,cmor_table)    # runid/table specific compliacne logs
    default_source_id   = 'CanESM5'                                     # default source id -> could maybe be a function arg
    if not nemo_mesh_mask_file:                                         # if the user didn't provide a nemo mesh mask file
        mask_pfx = 'sc_{runid}_{chunk}'.format(runid=runid,chunk=chunk)
        nemo_mesh_mask_file = "{}_mesh_mask.nc".format(mask_pfx)

    #-- define which tables should _only_ be converted if the given chunk contains N full calendar years (Jan-Dec)
    tables_requiring_full_calendar_years = [ "Oyr" ]
    if cmor_table in tables_requiring_full_calendar_years:
        if chunk_is_staggered(chunk):
            err_text  = "{} requires the chunk date range to begin in January and end in December!".format(cmor_table)
            err_text += " Incorrect values will be produced for the given chunk: {}.".format(chunk)
            err_text += " Halting table conversion.."
            pycmor_logfile.write("{}\n".format(err_text))
            raise Exception(err_text)

    #-- parse experiment table
    expdict = parse_exptable(exptable)
    if production: 
        # given runid must have an entry in the experiment table
        if not runid in expdict:
            err_text  = "{} not found in the given experiment table!".format(runid)
            err_text += " Production conversions require runids with entries to properly set metadata."
            err_text += " Halting conversion..."
            pycmor_logfile.write(err_text)
            raise Exception(err_text)

    #-- determine source id and try to load correct variable tables
    if runid in expdict:
        # get source id from experiment table
        source_id = expdict[runid]['Source ID']
    else:
        wrn_txt  = "WARNING: {} not found in given experiment table!\n".format(runid)
        wrn_txt += "Conversion will progress assuming that the source id is {}!\n".format(default_source_id)
        print(wrn_txt)
        pycmor_logfile.write(wrn_txt) 
        source_id = default_source_id

    if os.path.isdir(os.path.join(vtabdir,source_id)):
        # there is a directory pertaining to the desired source id
        source_vtabdir = os.path.join(vtabdir,source_id)
    else:
        # there is not a directory pertaining to the desired source id
        if production:
            err_text  = "{}'s source id ({}) does not have its own variable tables!\n".format(runid,source_id)
            err_text += " Production conversions must be done with officially supported source ids.\n"
            err_text += " Halting conversion..."
            pycmor_logfile.write(err_text)
            raise Exception(err_text)

        # check if the runid is attached to the source id and use the source id minus the runid if so
        #   i.e. if source id = CanESM5-runid, look for variable tables for CanESM5
        if source_id.endswith(runid):
            tmp_source_id = source_id.replace("-{}".format(runid),"")
            if os.path.isdir(os.path.join(vtabdir,tmp_source_id)):
                source_vtabdir = os.path.join(vtabdir,tmp_source_id)
            else:
                wrn_txt  = "WARNING: can not find variable tables for {}!\n".format(source_id)
                wrn_txt += "Conversion will progress using variable tables for {}\n".format(default_source_id)
                print(wrn_txt)
                pycmor_logfile.write(wrn_txt)
                source_vtabdir = os.path.join(vtabdir,default_source_id)
        else:
            # use tables associated with default source id
            wrn_txt  = "WARNING: can not find variable tables for {}!\n".format(source_id)
            wrn_txt += "Conversion will progress using variable tables for {}\n".format(default_source_id)
            print(wrn_txt)
            pycmor_logfile.write(wrn_txt)
            source_vtabdir = os.path.join(vtabdir,default_source_id)
    tab_dat = get_vartab(source_vtabdir,cmor_table)

    #-- init stats and dictionaries/lists to log what happens
    cccma_tbl_def_err   = 0             # includes unknown realms and no TS var defined
    cmor_tbl_mismatch   = 0             # cmip6/cccma cmor mismatch
    diagf_ndef          = 0             # no diagfile in the table
    ndiag_ncheck        = 0             # no diagfile AND no 'CCCma Check' text
    optdeck_pbl         = 0             # optional deck problems
    notfound            = 0             # input file not found by 'access'
    cmorfail            = 0             # CMOR exception raised
    pycmorfail          = 0             # pycmor exception raised
    attpt_conv          = 0             # attempted conversions
    no_excpt            = 0             # conversions completed with no exceptions
    notfound_vars       = []
    ndiag_ncheck_vars   = []
    cccma_tbl_def_err_vars  = {'log_name' : 'ccma_table_def_errors', 'vars': {}}
    cmor_tbl_mismatch_vars  = {'log_name' : 'cmor_table_mismatch', 'vars': {}}
    optdeck_pbl_vars        = {'log_name' : 'optional_deck_errors', 'vars': {}}
    cmorfail_vars           = {'log_name' : 'cmor_errors', 'vars' : {}}
    pycmorfail_vars         = {'log_name' : 'pycmor_errors', 'vars' : {}}
    if test_compliance:
        # track compliance errors/warnings
        PrePARE_errs,cfcheck_errs,cfcheck_wrns                  = 0,0,0
        PrePARE_errs_vars,cfcheck_errs_vars,cfcheck_wrns_vars   = [],[],[]
    if check_deltas:
        hash_deltas, frzn_hash_deltas               = 0,0
        hash_deltas_vars, frzn_hash_deltas_vars     = [],[]

    #-- init list for resulting files
    conv_files = []

    #-- create clean log dirs for this table
    mkclean_dir(pycmorlogtab_dir)
    if use_cmorvar_logfile: mkclean_dir(cmorlogtab_dir)
    if test_compliance: mkclean_dir(complogtab_dir)

    amor.logfile = os.path.join(amor.logfile_dir, 'amor_{}_{}.log'.format(runid,cmor_table))
    if os.path.exists(amor.logfile):
        remove_previous_logfile = True
        rename_previous_logfile = not True
        if remove_previous_logfile:
            os.remove(amor.logfile)  # remove previous amor.py logfile if it exists
        elif rename_previous_logfile:
            filestat = os.stat(amor.logfile)
            ts = filestat.st_ctime # local time
            shutil.move(amor.logfile,"{}_{}".format(amor.logfile,
                            datetime.datetime.utcfromtimestamp(ts).strftime('%Y-%m-%d_%H:%M:%S')))

    #-- if functionality turned on, load stored variable hashes for comparison
    if check_deltas:
        try:
            with open(ac.hash_file,'r') as f:
                stored_var_hashes = json.load(f)
        except IOError as exc:
            stored_var_hashes = {}
            err_str =  "check_deltas flag set to true, yet the stored hashfile ({}) doesn't exist.\n".format(ac.hash_file)
            raise Exception(err_str)

    # create dir where input files will be moved after processing
    cmor_files_dir = os.path.join( os.path.split(outpath.rstrip('/'))[0], 'input' )
    cmor_files_dir = 'input'
    if not os.path.exists(cmor_files_dir): os.makedirs(cmor_files_dir)
    # if outpath or cmor_files_dir aren't in current dir, for user's convenience create links in current dir to them
    cwd = os.path.normpath(os.getcwd())
    for path in [outpath, cmor_files_dir]:
        path = os.path.normpath(path)
        if not os.path.exists(path): continue
        head, tail = os.path.split(path)
        if head != cwd and not os.path.exists(tail):
            os.symlink(path, tail)

    # -- remove _metadata_ row
    if '_metadata_' in tab_dat:
        tab_dat.pop('_metadata_')

    # -- if given, limit the table data to variables in cmorvars (that exist in this table),
    #       else consider entire table
    if cmorvars:
        tmp = {}
        for var in cmorvars:
            if var in tab_dat.keys():
                tmp[var] = tab_dat[var]
                user_req_vars_in_tables.append(var)
        tab_dat = copy.deepcopy(tmp)
        
        if len(tab_dat) > 0:
            in_table_str = "\nFound user requested vars '{}' in {} table\n".format(
                                " ".join(tab_dat.keys()),cmor_table)
        else:
            in_table_str = "\nNo user requested vars located in {} table\n".format(
                                cmor_table)
        print(in_table_str)
        pycmor_logfile.write("{}\n".format(in_table_str))
        
    # -- check for variable exclusion for this runid
    if not no_exclusions:
        try:
            if expdict[runid]['Vars to exclude']:
                exclude_vars = expdict[runid]['Vars to exclude'].split(',')
                for evar in exclude_vars:
                    if evar in tab_dat.keys():
                        exclusion_str = "Excluding '{}' from conversion, as laid out by experiment table ".format(evar)
                        exclusion_str += "for runid {}\n".format(runid)
                        print(exclusion_str)
                        pycmor_logfile.write("{}\n".format(exclusion_str))
                        del tab_dat[evar]
        except KeyError as exc:
            wrn_str = "\nWarning: {} not found in experiment table, or is missing 'Vars to exclude'.\n".format(runid)
            pycmor_logfile.write(wrn_str)
    else:
        tmp_str  = "no_exclusions set to True. Attempting to convert all possible data for {}".format(runid)
        tmp_str += ", regardless of exclusions listed in experiment/variable tables.\n\n"
        pycmor_logfile.write(tmp_str)

    # -- Load the retracted variable tables
    if production:
        retracted_df = load_retracted_variables( basefile = retracted_basefile, fnames = retracted_addfiles )

    #===============
    # Variable loop
    #===============
    num_vars_to_convert = len(tab_dat)
    convert_rows = sorted(tab_dat.iteritems())
    print("Attempting to convert {} variables from CMOR table {}".format(num_vars_to_convert, cmor_table))

    for var, row in convert_rows:
        #==================
        # Variable Preamble
        #==================
        print('\n\n')
        print(' ')
        print('****************************************************************')
        print(' ')
        print("Working on CMOR variable: {}".format(var))

        # check for variable specific inclusion/exclusion settings
        if not no_exclusions:
            # define identifiers that we include/exclude by
            incl_excl_by = ['Experiment ID', 'Activity ID','Run ID']

            if row['Include only for'] and row['Include only for'].strip():
                include_for = [ entry.strip() for entry in row['Include only for'].split(',') ]

                # If none of this run's "incl_excl_by" values are contained in this column, skip it.
                if all([ not expdict[runid][idntfr] in include_for for idntfr in incl_excl_by]):
                    logtxt  = "'Include only' clause found for {} in {} variable table,\n ".format(var,cmor_table)
                    logtxt += "and the experiment/activity/Run ID for {} isn't included in the given list.. skipping conversion\n\n".format(runid)
                    pycmor_logfile.write(logtxt)
                    continue

            if row['Exclude for'] and row['Exclude for'].strip():
                exclude_for = [ entry.strip() for entry in row['Exclude for'].split(',') ]

                # If any of this run's "incl_excl_by" values are contained in this column, skip it.
                if any([expdict[runid][idntfr] in exclude_for for idntfr in incl_excl_by]):
                    logtxt  = "'Exclude for experiments' clause found for {} in {} variable table,\n".format(var,cmor_table)
                    logtxt += "and experiment/activity/Run ID for {} found in given list.. skipping conversion \n\n".format(runid)
                    pycmor_logfile.write(logtxt)
                    continue
        
        # start tracking conversion statistics
        attpt_conv +=1
        conv_files.append({'var' : var, 'file' : None})

        # store cmor_table in data structure to be used by create_user_table()
        #   - could maybe be moved to the get_ggle_ss.py script
        row['CMOR table'] = cmor_table

        #--- Check for presence of diagfile
        #       - skip conversion attempt if not present
        diagfile=row['CCCma diag file']
        if diagfile:
            diagfile = diagfile.strip().lower()
        else:
            # check if there is a value for 'CCCma check'
            #   - check for None type, empty string, or '?'
            if not row['CCCma check'] or not row['CCCma check'].strip() or row['CCCma check'].strip() == '?':
                logtxt          =  "Diag file not defined for {} (Priority {:.0f}) and no confirmation text in 'CCCma Check'\n".format(var,row['priority'])
                logtxt         += "\tAre we providing this variable?\n"
                ndiag_ncheck   += 1
                ndiag_ncheck_vars.append(var)
            else:
                logtxt = "Diag file not defined for {} (Priority {:.0f}). 'CCCma Check': {}\n".format(var,row['priority'],row['CCCma check'])

            # update stats and skip
            pycmor_logfile.write("{}\n".format(logtxt))
            diagf_ndef  += 1
            attpt_conv  -= 1
            continue

        #--- Check for CCCma time series name
        #       - skip conversion if not present
        if row['CCCma TS var name'] and row['CCCma TS var name'].strip():
            # Get TS variable names from the variable table. There might be one
            # or more, in which case they should be processed by an optional deck.
            tsvars      = row['CCCma TS var name'].strip().split(' ')
            tsvars      = list(filter(None, tsvars))         # remove empty strings.
            tsvars_L    = [ v.lower() for v in tsvars ]      # lower case needed in order to access files.
        else:
            # update stats and skip
            logtxt       = "TS var name not defined for {} (Priority {:.0f})\n".format(var,row['priority'])
            cccma_tbl_def_err_vars, cccma_tbl_def_err = log_err(logtxt,cccma_tbl_def_err,var,cccma_tbl_def_err_vars,pycmor_logfile)
            attpt_conv  -= 1
            continue

        #-- verify consistency with actual CMIP CMOR tables
        try:
            cc.verify_CMORtab({var : row}, cmor_table)
        except cc.CMORtableError as e:
            log_txt = "CMIP6/CCCma CMOR table mismatch encountered for {} (Priority {})\n".format(var,row['priority'])
            cmor_tbl_mismatch_vars, cmor_tbl_mismatch = log_err(log_txt, cmor_tbl_mismatch, var, cmor_tbl_mismatch_vars,
                                                                    pycmor_logfile, traceback.format_exc())
            continue

        #-- determine 'conversion_realm'
        try:
            conv_realm = deduce_conversion_realm(row)
        except Exception as exc:
            logtxt  = "Unable to determine conversion realm for {} (Priority {:.0f})\n".format(var,row['priority'])
            logtxt += "See below for details:\n"
            logtxt += "{}\n".format(traceback.format_exc())
            print(logtxt)
            cccma_tbl_def_err_vars, cccma_tbl_def_err = log_err(logtxt,cccma_tbl_def_err,
                                                            var,cccma_tbl_def_err_vars,pycmor_logfile)
            continue

        #-- access nemo mesh mask if its an ocean realm
        if conv_realm == 'ocean': 
            print("Using {} as the nemo mask file!".format(nemo_mesh_mask_file))
            if not os.path.exists(nemo_mesh_mask_file):
                result = access_file(nemo_mesh_mask_file, pycmor_logfile)
                if result == 1:
                    exc_str = "Failed to access {}! Are you sure this file was saved?".format(nemo_mesh_mask_file)
                    exc_str += " If not, specify a known mask via the -m flag in the pycmor call!"
                    raise Exception(exc_str)

        # -- initiate CMOR log file
        if use_cmorvar_logfile:
            # define variable specific log file
            cmor_logfile = "{}_cmor.log".format(var)
            cmor_logfile = os.path.join(cmorlogtab_dir, cmor_logfile)
        else:
            # print CMOR warnings and errors to stdout
            cmor_logfile = None

        #===========================================================
        # Configure/Run optional deck (if needed) and set input file
        #===========================================================
        pfx         = 'sc_{runid}_{chunk}'.format(runid=runid,chunk=chunk)
        diagfiles   = diagfile.split(" ")
        optdeck     = row['CCCma optional deck'].strip() if row['CCCma optional deck'] else None # The CCCma deck
        out         ='derived'           # Standard output suffix


        if optdeck:
            # overwrite the output suffix if desired (typically ONLY done for surface air pressure)
            if 'optdeck output suffix' in row:
                out = row['optdeck output suffix']
        
            # run optional deck and log any errors that occur
            success,file4cmor,optdeck_logtxt = run_optional_deck(optdeck, tsvars_L, diagfiles, pfx, conv_realm, 
                                                                    out_suff=out, nemo_mask=nemo_mesh_mask_file)
            if not success:
                logtxt  = "Optional deck failed for CMOR var {} (Priority {:.0f})\n".format(var,row['priority'])
                logtxt += optdeck_logtxt
                print(logtxt)
                optdeck_pbl_vars, optdeck_pbl = log_err(logtxt,optdeck_pbl,var,optdeck_pbl_vars, pycmor_logfile)
                continue
            else:
                pycmor_logfile.write(optdeck_logtxt)
        else:
            # check if an optional deck SHOULD have been applied
            if not len(tsvars) == 1 or not len(diagfiles) == 1:
                # There should not be multiple input variables or diag files without an optional deck
                logtxt  = "Multiple input variables or diag files defined for {} (Priority {:.0f}), but no optional deck.\n".format(var,row['priority'])
                logtxt += "\t input vars: {} \n\t diagfiles: {} .... skipping\n".format(" ".join(tsvars), diagfile)
                optdeck_pbl_vars, optdeck_pbl = log_err(logtxt,optdeck_pbl,var,optdeck_pbl_vars,pycmor_logfile)
                continue

            # -- set CMOR input file and access it
            pfx += "_{diagfile}".format(diagfile=diagfile)
            file4cmor = "{}_{}".format(pfx,tsvars_L[0])
            # After CMIP6 the following line should be deleted and replaced with # if conv_realm == 'ocean': file4cmor += ".nc"
            if conv_realm == 'ocean': file4cmor = pycmor_to_deprecate.detect_nc_or_not(file4cmor)

            result = access_file(file4cmor)
            if result == 1:
                # The file was not accessed successfully.
                logtxt = 'failed to access input file {} for var {} (Priority {:.0f})\n'.format(file4cmor,var,row['priority'])
                pycmor_logfile.write(logtxt)
                print(logtxt)
                notfound += 1
                notfound_vars.append(var)
                continue

        #===============
        # Run conversion
        #===============
        # -- create user table
        successful_conv = False
        cmor_final_filename = ''
        var_version = default_version
        # In production mode, the default version might need to be overridden
        if production:
            var_version = set_var_version( var, cmor_table, expdict[runid], retracted_df, default_version, new_version )
        user_f, setusertab_err = create_user_table(runid, row, conv_realm, model_hash=model_hash,
                                        expdict=expdict, outpath=outpath, version=var_version,
                                        production=production)
        pycmor_logfile.write(setusertab_err)

        # -- Convert ocean Realms
        if conv_realm == 'ocean':

            # open the nemo_mesh_mask file (accessed about and potentially used by optional decks)
            nemo_grid = xr.open_dataset(nemo_mesh_mask_file)
            # Here we don't strip out the last z level because it is necessary to calculate the correct cell bound
            # for the depth cordinate
            nemo_grid = nmor.strip_extra_row_cols(nemo_grid, has_x = True, has_y = True, has_z = False)

            try:
                # Read the dataset
                vardata = xr.open_dataset(file4cmor, decode_times = False)

                # call nemo_to_cmor with args from the variable table
                successful_conv = nmor.nemo_to_cmor('CMIP6_' + cmor_table + '.json',
                                                     str(var),
                                                     vardata,
                                                     file4cmor,
                                                     str(tsvars[0]), # first index hard coded to deal with how optional deck operations name the variable
                                                     nemo_grid,
                                                     row,
                                                     positive='down',
                                                     user_f=user_f,
                                                     cmor_logfile=cmor_logfile
                                                    )

                cmor_final_filename = cmor.get_final_filename()

            # Handle exceptions
            except Exception as exc:
                errtxt = "{}\n{}\n".format(exc,traceback.format_exc())
                if type(exc) == cmor.CMORError:
                    logtxt = "CMOR exception on {} (Priority {:.0f})\n".format(var,row['priority'])
                    cmorfail_vars,cmorfail = log_err(logtxt,cmorfail,var,cmorfail_vars,pycmor_logfile,errtxt)
                else:
                    logtxt = "pycmor exception on {} (Priority {:.0f})\n".format(var,row['priority'])
                    pycmorfail_vars,pycmorfail = log_err(logtxt,pycmorfail,var,pycmorfail_vars,pycmor_logfile,errtxt)
                print(logtxt)
                print(errtxt)

            # close datasets, regardless if an exception occured or not
            finally:
                vardata.close()
                nemo_grid.close()

        #-- AGCM realms
        elif conv_realm == 'atmos':
            # call agcm_to_cmor with args from the variable table
            d = {
               'cmortable'     : 'CMIP6_' + str(cmor_table) + '.json',
               'cmorvar'       : str(var),
               'source_ctabdir': source_vtabdir,
               'cccunits'      : str(row['units']),
               'cccts'         : str(file4cmor),
               'cccvar'        : str(tsvars[0]),
               'cccdiagfile'   : diagfile,
               'chunk'         : str(chunk),
               'dimensions'    : str(row['dimensions']),
               'frequency'     : str(row['frequency']),
               'optdeck'       : optdeck,
               'user_f'        : str(user_f),
               'ccchistory'    : str(row['CCCma optional deck']),
               'ccccomment'    : str(row['CCCma comment']),
               'cmor_logfile'  : cmor_logfile,
               'production'    : production
            }

            if True:
                # should remove this later?
                # correct info should in the spreadsheet
                # or, the opt deck determines the units?
                if optdeck:
                    if optdeck in ['degctok']: d['cccunits'] = 'K'

            amor_queue = multiprocessing.Manager()
            d_return = amor_queue.dict()
            d['d_return'] = d_return
            try:
                amor_conversion_process = multiprocessing.Process(
                        target=amor.agcm_to_cmor,
                        kwargs=d)
                amor_conversion_process.start()
                amor_conversion_process.join()
                successful_conv     = d_return['success']
                cmor_final_filename = d_return['cmor_final_filename']
            except Exception as exc:
                amor_exception_handler(exc, d_return)

            if 'exc_type' in d_return: # indicates an exception occurred
                #if type(exc) == cmor.CMORError:
                errtxt = d_return['errtxt']
                if d_return['exc_type'] == 'CMORError':
                    logtxt = "CMOR exception on {} (Priority {:.0f})\n".format(var,row['priority'])
                    cmorfail_vars,cmorfail = log_err(logtxt,cmorfail,var,cmorfail_vars,pycmor_logfile,errtxt)
                else:
                    logtxt = "pycmor exception on {} (Priority {:.0f})\n".format(var,row['priority'])
                    pycmorfail_vars,pycmorfail = log_err(logtxt,pycmorfail,var,pycmorfail_vars,pycmor_logfile,errtxt)
                print(logtxt)
                print(errtxt)

        # cleanup input files
        if os.path.exists(file4cmor):
            os.system('mv ' + file4cmor + ' ' + cmor_files_dir)

        # log successful conversions
        if successful_conv:
            no_excpt += 1
            logtxt = "Converted {} without exceptions\n\n".format(var)
            pycmor_logfile.write(logtxt)
            cmor_out_fl = cmor_final_filename
            conv_files[-1]['file'] = cmor_out_fl

            if test_compliance:
                # test compliance on outputted file
                complog     = os.path.join(complogtab_dir,"{}.log".format(var))
                compstats   = cc.test_compliance(cmor_out_fl,complog)
                if compstats['PrePARE_errs']['nfiles'] > 0:
                    PrePARE_errs += 1; PrePARE_errs_vars.append(var)
                if compstats['cfcheck_errs']['nfiles'] > 0:
                    cfcheck_errs += 1; cfcheck_errs_vars.append(var)
                if compstats['cfcheck_wrns']['nfiles'] > 0:
                    cfcheck_wrns += 1; cfcheck_wrns_vars.append(var)

        # check memory usage
        pycmor_mem_usage = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
        subprc_mem_usage = resource.getrusage(resource.RUSAGE_CHILDREN).ru_maxrss
        print("Current pycmor RAM usage, after {} var {} = {} kb".format(conv_realm, var, pycmor_mem_usage))
        print("Current subproc RAM usage, after {} var {} = {} kb".format(conv_realm, var,subprc_mem_usage))

    #=======================================
    # Check for hash deltas in output arrays
    #=======================================
    if check_deltas:
        for dct in conv_files:
            var         = dct['var']
            cmor_out_fl = dct['file']

            # conv_key defines a unique conversion identifier
            conv_key = "{}-{}-{}-{}-{}".format(source_id,var,cmor_table,runid,chunk)
            var_hash_info, var_hash_delta = ac.check_delta(cmor_out_fl, conv_key,
                                                            stored_var_hashes=stored_var_hashes)
            if var_hash_delta:
                hash_deltas += 1 ; hash_deltas_vars.append(var)
                if var_hash_info['frozen']:
                    frzn_hash_deltas += 1; frzn_hash_deltas_vars.append(var)

            # store new hash_info
            var_hashes[conv_key] = var_hash_info

    #===============================
    # Clean up any timeseries files
    #===============================
    for ts_file in glob.glob("sc_*"):
        print("Cleaning {}".format(ts_file))
        os.remove(ts_file)

    #===================================
    # Output stats and create final logs
    #===================================
    stats   =  '\n-----------------------------------------\n'
    stats   += 'pycmor stats:\n'
    stats   += '\tno. of variables inspected:             \t{num_vars_to_convert}\n'.format(num_vars_to_convert=num_vars_to_convert)
    stats   += '\tno. vars with undefined diag files:     \t{diagf_ndef}\n\n'.format(diagf_ndef=diagf_ndef)

    stats   += '\tno. vars with undefined diag files AND \n'
    stats   += '\tw/o CCCma Check:                        \t{ndiag_ncheck}\n\n'.format(ndiag_ncheck=ndiag_ncheck)

    stats   += '\tAttempted conversions:                  \t{attpt_conv}\n'.format(attpt_conv=attpt_conv)
    stats   += '\tErrors:\n'
    stats   += '\t\tCMIP6/CCCma CMOR table inconsistencies: {cmor_tbl_mismatch}\n'.format(cmor_tbl_mismatch=cmor_tbl_mismatch)
    stats   += '\t\tCCCma table definition errors:          {cccma_tbl_def_err}\n'.format(cccma_tbl_def_err=cccma_tbl_def_err)
    stats   += '\t\toptional deck pblms:                    {optdeck_pbl}\n'.format(optdeck_pbl=optdeck_pbl)
    stats   += '\t\tnot found:                              {notfound}\n'.format(notfound=notfound)
    stats   += '\t\tCMOR exceptions:                        {cmorfail}\n'.format(cmorfail=cmorfail)
    stats   += '\t\tpycmor exceptions:                      {pycmorfail}\n\n'.format(pycmorfail=pycmorfail)
    stats   += '\tvars converted w/o exceptions:          \t{no_excpt}\n'.format(no_excpt=no_excpt)
    if test_compliance:
        stats += '\n-----------------------------------------\n'
        stats += 'compliance stats on output files:\n'
        stats += '\tno. output files with PrePARE errors:   {PrePARE_errs}\n'.format(PrePARE_errs=PrePARE_errs)
        stats += '\tno. output files with cfcheck errors:   {cfcheck_errs}\n'.format(cfcheck_errs=cfcheck_errs)
        stats += '\tno. output files with cfcheck warnings: {cfcheck_wrns}\n'.format(cfcheck_wrns=cfcheck_wrns)
    if check_deltas:
        stats += '\n-----------------------------------------\n'
        stats += 'variable array hash update stats:\n'
        stats += '\tno. updated hashes:                 {hash_deltas}\n'.format(hash_deltas=hash_deltas)
        stats += '\tno. FROZEN updated hashes (BAD):    {frzn_hash_deltas}\n'.format(frzn_hash_deltas=frzn_hash_deltas)
        if not frzn_hash_deltas == 0:
            stats += '\n\t#======================================================================================#'
            stats += '\n\t# WARNING: AT LEAST ONE CALCULATED VARIABLE HASH HAS DIVERGED FROM THE STORED VERSION! '
            stats += '\n\t#          IS THIS EXPECTED?! IF SO, UPDATE THE STORED HASHES!                         '
            stats += '\n\t#                                                                                      '
            stats += '\n\t# See {} for a list of the updated frozen variables.                                   '.format(
                            os.path.join(pycmorlogtab_dir,'frozen_hash_deltas.log'))
            stats += '\n\t#======================================================================================\n\n'

    # create pycmor log files
    # -- dictionaries
    for dct in [cccma_tbl_def_err_vars,optdeck_pbl_vars,cmorfail_vars,pycmorfail_vars,cmor_tbl_mismatch_vars]:
        # only output log if errors occurred for this category
        if not len(dct['vars']) == 0:
            pycmorlog_f     = "{}.log".format(dct['log_name'])
            pycmorlog_json  = "{}.json".format(dct['log_name'])
            pycmorlog_f     = os.path.join(pycmorlogtab_dir,pycmorlog_f)
            pycmorlog_json  = os.path.join(pycmorlogtab_dir,pycmorlog_json)
            # dump to json file
            with open(pycmorlog_json,'w') as f:
                json.dump(dct, f, indent=2, sort_keys=True)
            # write txt file log in readable format
            with open(pycmorlog_f,'w') as f:
                for v, txt in dct['vars'].iteritems():
                    f.write("\n{}:\n".format(v))
                    f.write("\t{}".format("\n\t".join(txt.split("\n"))))
                    f.write("\n")

    # -- lists
    var_lists = [(notfound_vars,"not_found.log"),(ndiag_ncheck_vars,"nodiag_nocheck.log")]
    if check_deltas:
        var_lists.extend([(hash_deltas_vars,"hash_deltas.log"),(frzn_hash_deltas_vars,"frozen_hash_deltas.log")])
    for lst,fl in var_lists:
        # only output log if errors occurred for this category
        if lst:
            pycmorlog_f = os.path.join(pycmorlogtab_dir,fl)
            with open(pycmorlog_f,'w') as f:
                for v in lst: f.write("{}\n".format(v))
    if test_compliance:
        for lst,fl in ((PrePARE_errs_vars,"PrePARE_errs.log"),(cfcheck_errs_vars,"cfcheck_errs.log"),
                        (cfcheck_wrns_vars,"cfcheck_wrns.log")):
            if lst:
                pycmorlog_f = os.path.join(pycmorlogtab_dir,fl)
                with open(pycmorlog_f,'w') as f:
                    for v in lst: f.write("{}\n".format(v))

    print("\nSee {} for pycmor specific log files.".format(pycmorlogtab_dir))
    if use_cmorvar_logfile:
        print('\nSee {} for CMOR specific log files'.format(cmorlogtab_dir))
        stats += summarize_CMOR_output(cmorlogtab_dir)

    pycmor_logfile.write(stats)
    pycmor_logfile.close()

def chunk_is_staggered(chunk_string):
    """
        Take in a chunk string of the form YYYY_YYYY or YYYYmm_YYYYmm and check if it
        contains clean, full calendar years (i.e. starts in January and end in December),
        returning True if so, and False if not.

        Note: if months aren't included we start date is in January and the final date is in December

        Parameters
        ----------
            chunk_string : str
                Date string in the form of YYYY_YYYY or YYYYmm_YYYYmm

        Returns
        -------
            _chunk_is_staggered : bool
                True if date string contains a clean multiple of calendar years, False if not.
    """
    start_date, end_date = chunk_string.split("_")

    # determine start month
    if len(start_date) == 4 and len(end_date) == 4:
        # only given years, assume jan start/dec end
        _chunk_is_staggered = False
    elif len(start_date) == 6 and len(end_date) == 6:
        # given YYYYMM strings - check months
        start_month = int(start_date[-2:])
        end_month   = int(end_date[-2:])
        if ( start_month == 1 ) and ( end_month == 12 ):
            _chunk_is_staggered = False
        else:
            _chunk_is_staggered = True
    else:
        raise ValueError("Unsupported chunk string: {}.\n Expecting YYYY_YYYY, or YYYYmm_YYYYmm".format(chunk_string))
    return _chunk_is_staggered

def amor_exception_handler(exc, d):
    """
    Get info on an exception that occured and store it in dict 'd'.
    This is called within amor.py to allow it to return info to table_utils
    giving info on the exception. Otherwise table_utils wouldn't have access
    to this info.

    Parameters
    ----------
        exc : Exception
            Object indicating the exception that occurred
        d : dict
            Used to return info about the exception
    Returns
    -------
        Nothing (input dict 'd' is used to hold the returned info).
    """
    errtxt = "{}\n{}\n".format(exc,traceback.format_exc())
    d.update({'exc_type' : exc.__class__.__name__, 'errtxt' : errtxt})
    # The exc class name is passed back because we cannot pass the exc object back
    # (e.g. kwargs['d_return']['exc'] = exc) due to this error:
    #   PicklingError: Can't pickle <class '_cmor.CMORError'>: import of module _cmor failed
    # I believe (not sure) this is because under the hood the multiprocessing module passes
    # stuff back to thread_utils by pickling it.

def log_err(logtxt, cntr, var, dct, logf_obj, add_txt = None):
    """ Log error information.

        Store information about error that occurred by writing the defined text
        to a file object (``logf_obj``); incrementing a counter (``cntr``)
        associated with this error type; and finally, store log text (``logtxt``)
        in a dictionary (``dct``) so it can be presented in an organized format
        at a later time.

        Parameters
        ----------
            logtxt : str
                text to be stored in the dictionary
            cntr : int
                original counter value
            var : str
                variable name
            dct : dict
                dictionary storying the information
            logf_obj : file object
                object to write the text to
            add_txt : str **optional**
                additional text that should be added to logtxt after
                it is written to log file object, but before it is stored in
                the dictionary.

        Returns
        -------
            dct : dict
                updated dictionary
            error_cntr : int
                updated counter value
    """
    # write to logfile object
    logf_obj.write("{}\n".format(logtxt))

    # if add_txt provided, add to log_txt
    if add_txt:
        logtxt += add_txt

    # increment counter and store variable error text in dictionary
    cntr += 1
    dct['vars'][var] = logtxt
    return dct, cntr

def summarize_CMOR_output(logdir):
    """
        Given the log directory, this function parses the output files from CMOR and
        provides useful output to the user.

        .. note:: 

            this function assumes that the logfiles are named as ${varname}_*

        Parameters
        ----------
            logdir : str
                directory containing the log files. Note, this function does
                not do a recursive search, so the files must live in this
                directory.

        Returns
        -------
            summary : str
                summary text about the log files.
    """
    logfiles = os.listdir(logdir)

    # count how many log files were produced, and determine the distribution of errors and warnings
    vars_wth_err = []
    vars_wth_wrn = []
    for logfile in logfiles:
        var = logfile.split('_')[0]    # extract variable name

        # check its log for warnings/errors
        logfile_apath = os.path.join(logdir,logfile)
        with open(logfile_apath) as f:
            lg_txt = f.read()
        if 'Error:' in lg_txt: vars_wth_err.append(var)
        if 'Warning:' in lg_txt: vars_wth_wrn.append(var)

    summary =  '\n-----------------------------------------\n'
    summary += 'CMOR output stats:\n'
    summary += '\tCMOR logs produced:             {}\n'.format(len(logfiles))
    summary += '\tNo. logs with CMOR warnings:    {}\n'.format(len(vars_wth_wrn))
    summary += '\tNo. logs with CMOR errors:      {}\n'.format(len(vars_wth_err))
    return summary

def access_file(fn, logfile=None):
    """ Tries to access file fn, and writes errors to logfile, returning 1 if 
        the ``access`` call failed, and 0 on success.

        Parameters
        ----------
            fn : str
                file to access
            logfile : str **optional**
                log file to write error output to. Defaults to ``None``.
    """
    print "trying to access {}".format(fn)
    subprocess.call(['access', fn, fn])

    if not os.path.exists(fn):
    # verify the output file was created
        if logfile: logfile.write("Failed to access: {}\n\n".format(fn))
        return 1
    else:
        return 0

def create_user_table(runid, vardat, realm, model_hash="Unknown", user_f_template='cccma_user_input.json', CMIP6_CV_loc='CMIP6_CVs',
                        expdict=None, outpath='output', version='version', production=False):
    """
        Using the variable table, experiment table, and the CMIP6 source id CV, create custom
        user table for this variable, using the template provided.

        Parameters
        ----------
            runid : str
                runid for the experiment that produced the data
            vardat : dict
                dictionary containing information for the specified variable
                (should come from variable table)
            realm : str
                realm for which to extract grid info to store in the user table
            model_hash : str **optional**
                source code hash for the runid in question. Defaults to "Unknown"
            user_f_template : str **optional**
                default file used as template for the user table (information
                from exp and var tables will be used to modify some of these
                values). Defaults to ``cccma_user_input.json``.
            CMIP6_CV_loc : str **optional**
                directory where the CMIP6 controlled vocabulary files are stored.
                Defaults to ``CMIP6_CVs``.
            expdict : dict **optional**
                if given, uses the information in this dictionary to set
                experiment information in user table. Defaults to 
                ``None``.
            outpath : str **optional**
                directory where the converted files will be stored. Defaults to
                ``output``.
            version : str **optional**
                version string associated with the conversion. Defaults to 
                ``version``.
            production : bool **optional**
                if true, create "production" user table, which means that no
                Disclaimer about preliminary data is included in the resulting
                tables. Defaults to ``False``.

        Returns
        -------
            user_f_output : str
                modified user json filename
            err_log : str
                log of errors that occur during the process
    """

    # initiate useful vars
    cwd             = os.getcwd()                                           # current directory
    pycmordir       = os.path.dirname(os.path.realpath(__file__))           # directory of running script
    err_log         = ""                                                    # error log
    usertab_dir     = "user_tables"                                         # user table directory
    user_f_output   = "cccma_{runid}_{CMORtable}_{CMORname}_user_input.json".format(    # user table filename
                        runid=runid, CMORtable=vardat['CMOR table'], CMORname=vardat['CMOR Name'])
    user_f_output   = os.path.join(usertab_dir,user_f_output)

    # create dir for user json files
    if not os.path.isdir(usertab_dir): os.makedirs(usertab_dir)

    # get template data
    with open(user_f_template, 'r') as f:
        user_tab = json.load(f)

    # add Disclaimer if production is False
    if not production:
        Disclaimer = "PRELIMINARY DATA - NOT FOR PUBLICATION."
        user_tab['DISCLAIMER'] = Disclaimer

    # replace with pertinent experiment info
    source_id_f = os.path.join(CMIP6_CV_loc,"CMIP6_source_id.json")
    user_tab, setexp_err = set_exp_info(user_tab, runid, realm, model_hash=model_hash, 
                                        expdict=expdict, source_id_f=source_id_f, production=production)
    err_log += setexp_err

    # set pycmor hash to n/a for inline conversions as the version is controlled by the CanESM version
    #   NOTE this change should ONLY be contained in the subtree version of ncconv within 
    #        CCCma_tools (i.e. it shouldn't be propogated back to nnconv
    user_tab['CCCma_pycmor_hash'] = "not applicable"

    # store out-path and version info
    if isinstance(outpath, (str, unicode)):
        # Specify output dir where CMOR-ized netcdf files will be placed
        if len(outpath) > 0:
            if not os.path.exists(outpath): os.makedirs(outpath)
            user_tab['outpath'] = outpath
    if isinstance(version, (str, unicode)):
        # Specify the final component of the pathname
        if len(version) > 0: user_tab['version'] = version

    # write new file
    with open(user_f_output, 'w') as f:
        json.dump(user_tab, f, indent=4, sort_keys=True) # indenting and sorting make the json file readable by humans
        print('Wrote file: ' + user_f_output)

    return user_f_output, err_log

def set_exp_info(user_tab, runid, realm, model_hash="Unknown", expdict=None, exptable='cmip6_experiments/cmip6_experiment.txt',
                    source_id_f="CMIP6_CVs/CMIP6_source_id.json", production=False):
    """
        Take in an existing user table and replace values with information defined in experiment table

        Parameters
        ----------
            user_tab : dict
                dictionary containing the user table info to be modified
            runid : str
                runid for the experiment that produced the data
            realm : str
                the realm for which to extract grid information for (eg: ``ocean``, ``atmos``) 
            model_hash : str **optional**
                the source code hash for the runid in question
            expdict : dict
                dictionary containing information on various experiments
            exptable : str
                path to the table file containing info for CCCma
                experiments.  Only used if expdict isn't given.
            source_id_f : str
                path the to file containing cmor CV file, which should contain grid information for
                the source id in question
            production : bool
                tells function to raise exception or not, if it can't find the desired source id in
                source_id_f

        Returns
        -------
            updated_user_tab : dict
                dictionary containing the updated user table
            err_log : str
                string that contains error logging

    """
    # get static calendar and ref time from user table
    ref_time = user_tab['#time_axis_origin']
    calendar = user_tab['calendar']

    # define trivial key pairs to be replaced in new user table
    #   keys    : user table keys to be replaced
    #   values  : experiment info to set in user table
    replace_dict = {
        'activity_id'       : 'Activity ID',
        'experiment_id'     : 'Experiment ID',
        'sub_experiment_id' : 'sub Experiment ID',
        'source_id'         : 'Source ID',
        'run_variant'       : 'Variant Info',
        'comment'           : 'Comment',
                    }

    # define variant info tuple
    variant_indices = [ ('realization_index','r'),
                        ('initialization_index','i'),
                        ('physics_index','p'),
                        ('forcing_index','f') ]

    # init error log and updated user table
    err_log = ""
    updated_user_tab = user_tab.copy()

    #===========================================================
    # Store the model hash for the run being converted, if known
    #===========================================================
    # Note: this is a CCCma specific field
    print("Setting meta-data CCCma_model_hash to {}".format(model_hash))
    updated_user_tab['CCCma_model_hash'] = model_hash

    #=========================
    # extract experiment info
    #=========================
    if not expdict:
        expdict = parse_exptable(exptable)
    try:
        runid_dat = expdict[runid]
    except KeyError as exc:
        err_log += "Runid {} not found in experiment table!\n".format(runid)
        err_log += "\t Not setting experiment info\n\n"
        return updated_user_tab, err_log

    #============================
    # execute simple replacements
    #============================
    for user_key, exp_key in replace_dict.iteritems():
        updated_user_tab[user_key] = runid_dat[exp_key]

    #=====================================
    # perform "more involved" replacements
    #=====================================
    # realization, initialization, physics, forcing
    expvariant = runid_dat['Variant Label']
    for var_inf in variant_indices:
        # regex looks for variant index id and matches the index number after it
        reg_pat = "(?<={})\d+".format(var_inf[1])
        ind     = re.search(reg_pat,expvariant).group(0)
        updated_user_tab[var_inf[0]] = ind

    # parent experiment ID and variant label
    parent_runid = runid_dat['Parent Run ID']
    if not parent_runid == 'none':
        try:
            parent_srcID    = expdict[parent_runid]['Source ID']
            parent_expID    = expdict[parent_runid]['Experiment ID']
            parent_actvID   = expdict[parent_runid]['Activity ID']
            parent_varlbl   = expdict[parent_runid]['Variant Label']
            updated_user_tab['parent_source_id']     = parent_srcID
            updated_user_tab['parent_experiment_id'] = parent_expID
            updated_user_tab['parent_activity_id']   = parent_actvID
            updated_user_tab['parent_variant_label'] = parent_varlbl
        except KeyError as exc:
            if production:
                exp_text  = "Could not find the parent runid ---> {} <--- ".format(parent_runid)
                exp_text += "in {} ".format(exptable)
                raise Exception(exp_text)

            err_log += "\n*********** WARNING *************\n"
            err_log += "Parent runid {} not found in {}\n".format(parent_runid,exptable)
            err_log += "Failed to set 'parent_experiment_id' and 'parent_variant_label' in user table\n"
            err_log += "*********************************\n\n"

    # set branch time information
    #   -- get experiment table info
    exptbl_brncht_prnt = runid_dat['Branch Time in Parent']
    exptbl_brncht_chld = runid_dat['Branch Time in Child']

    #   -- convert to floats (consistent units/calendar in static user table)
    tmp = conv_str2date(exptbl_brncht_prnt)
    flt_brncht_prnt = cftime.date2num(tmp,ref_time,calendar)
    tmp = conv_str2date(exptbl_brncht_chld)
    flt_brncht_chld = cftime.date2num(tmp,ref_time,calendar)

    #   -- set required values in updated user table
    updated_user_tab['branch_time_in_parent']   = flt_brncht_prnt
    updated_user_tab['branch_time_in_child']    = flt_brncht_chld
    updated_user_tab['parent_time_units']       = ref_time

    # model grid information
    #   -- get source id info from pertinent CV file
    source_id = runid_dat['Source ID']
    with open(source_id_f,'r') as f:
        source_id_dct    = json.load(f)

    #   -- confirm that the given source id is logged in the CV
    if not source_id in source_id_dct['source_id']:
        if production:
            err_text = "The given Source ID {} is not present in {}!".format(source_id,source_id_f)
            err_text += "\nHalting conversion...\n"
            raise Exception(err_text)
        else:
            err_log += "WARNING: Failed to find {} in {}! ".format(source_id,source_id_f)
            err_log += " Defaulting to using 'CanESM5'...\n"
            source_id = "CanESM5"

    #   -- extract component information
    source_comp_info = source_id_dct['source_id'][source_id]['model_component'][realm]

    # -- extract grid info (this regex searches for text between parentheses)
    grid = re.search('(?<=\().*(?=\))',source_comp_info['description']).group(0)

    # -- set grid and nominal resolution
    updated_user_tab['grid']                = grid
    updated_user_tab['nominal_resolution']  = source_comp_info['native_nominal_resolution']

    #====================================================================
    # add some additional CCCma specific fields from the experiment table
    #====================================================================
    # runid and parent runid
    updated_user_tab['CCCma_runid']         = runid
    updated_user_tab['CCCma_parent_runid']  = parent_runid

    # readable strings for the branch time
    updated_user_tab['YMDH_branch_time_in_parent'] = exptbl_brncht_prnt
    updated_user_tab['YMDH_branch_time_in_child']  = exptbl_brncht_chld

    return updated_user_tab, err_log

def get_model_hash(runid, source_location=None, run_user=None, events_database=None, 
                    runlog_dir=_default_runlog_dir, production=False):
    """
        Attempt to populate the model hash for the run being converted, going through a series of
        internal methods:
        1. _get_hash_from_source()
        2. _get_hash_from_events_db()
        3. _get_hash_from_runlogs()

        Parameters
        ----------
            runid : str
                runid of interest 
            source_location : str **optional**
                directory containing the source code for the CanESM run. If None, ``get_model_hash``
                constructs it from ``CCRNSRC`` or the _source_location_template module variable
            run_user : str **optional**
                user account that is running the model. If None, assumes the user running the conversion
                is the user account who ran the model.
            events_database : str **optional**
                sqlite file that contains a log of the "events" that a run went through, throughout the running
                process. If None, ``get_model_hash`` constructs it from ``CCRNSRC`` or the _run_event_database_template
                module variable.
            runlog_dir : str **optional**
                dictory containing the runlogs. Defaults to
                ``/home/scrd104/public_html/canesm-run-logs``
            production : bool **optional**
                if True, means this is a production conversion and will cause ``get_model_hash`` to 
                throw an exception if it can't find the given runid's hash information

        Returns
        -------
            model_hash : str
                the model hash associated with the latest version used for the desired runid.
    """
    #========================
    # Define internal methods
    #========================
    def _get_hash_from_source():
        """
            Query the source repo
        """
        _model_hash = None
        print("\tAttempting to set model hash by querying {}".format(source_location))
        if os.path.exists(source_location):
            try: 
                _model_hash = subprocess.check_output(['git','--git-dir','{}/.git'.format(source_location),'rev-parse','HEAD']).strip()
                print("\tSet model hash to {}".format(_model_hash))
            except subprocess.CalledProcessError:
                print('\tFailed to query {} for the model hash! Command output'.format(source_location))
                print('\t{}'.format(traceback.format_exc()))
        else:
            print("\tThe given source location, {}, does not exist.".format(source_location))
        if _model_hash is None:
            print("\tFailed to set model hash from its source code")
        else:
            _model_hash = _model_hash.strip()
        return _model_hash

    def _get_hash_from_events_db():
        """
            Query the events database file for this run, in the assumed location
        """
        _model_hash = None
        print("\tAttempting to set model hash by querying its events database {}".format(events_database))
        if os.path.isfile(events_database): 
            conn = sqlite3.connect(events_database)    
            cursor = conn.cursor()
            # query database to select most recent hash
            db_query = "SELECT CanESM_SHA1 FROM events ORDER BY Date DESC LIMIT 1;"
            cursor.execute(db_query)
            sha_entries = cursor.fetchall() 
            if len(sha_entries) == 1:
                # should return a single element list with a single element tuple
                _model_hash = sha_entries[0][0]
                print("\tSet model hash to {}".format(_model_hash))
            else:
                print("\tFailed to pull most recent event for {runid} from events database".format(runid=runid))
                print("\t{database}".format(database=events_database))
            conn.close()
        else:
            print("\tThe given events database, {}, does not exist.".format(events_database))
        if _model_hash is None:
            print("\tFailed to set model hash from its events database")
        else:
            _model_hash = _model_hash.strip()
        return _model_hash

    def _get_hash_from_runlogs():
        """
            Query the runlog files. This is used as a last resort, as it can result in conversion
            failures if any other process is working on these text files.
        """
        _model_hash = None
        runid_log = os.path.join(runlog_dir,runid,'table')  # path to runlog table
        runlog_sha_cell_ind = 3                             # column of runlog table containing hash string
        print("\tAttempting to set model hash by querying run log table {}".format(runid_log))
        # make sure log file actually exists
        if os.path.isfile(runid_log):
            # extract rows from runlog table
            with open(runid_log,'r') as f:
                log_txt = f.read()
            rows = re.findall('<TR>.*?</TR>',log_txt,re.DOTALL)

            # extract CanESM5 hash
            cells = re.findall('(?<=<TD>).*(?=</TD>)',rows[-1])
            try:
                _model_hash = cells[runlog_sha_cell_ind]
                print("\tSet model hash to {}".format(_model_hash))
            except IndexError as exc:
                print("\tFailed to pull model hash from its run log table!")
        else:
            print("\tThe runlog file {} does not exist!".format(runid_log))
        if _model_hash is None:
            print("\tFailed to set model hash from its run log table")
        else:
            _model_hash = _model_hash.strip()
        return _model_hash

    #====================
    # Initiate parameters
    #====================
    model_hash = None
    methods = [ _get_hash_from_source, 
                _get_hash_from_events_db, 
                _get_hash_from_runlogs ]

    if run_user is None:
        run_user = getpass.getuser() 
    if source_location is None:
        # need to avoid the deprecated "standard" environment
        if ("CCRNSRC" in os.environ) and (not os.environ['CCRNSRC'] == "/home/scrd101"):
            source_location = os.path.join(os.getenv('CCRNSRC'),'CanESM') 
        else:
            source_location = _source_location_template.format(runid=runid, user=run_user)
    if events_database is None:
        # need to avoid the deprecated "standard" environment
        if ("CCRNSRC" in os.environ) and (not os.environ['CCRNSRC'] == "/home/scrd101"):
            events_database = os.path.join(os.getenv('CCRNSRC'),'..','{runid}-log.db'.format(runid=runid)) 
        else:
            events_database = _run_event_database_template.format(runid=runid, user=run_user)

    #==============
    # Get the Hash!
    #==============
    for func in methods:
        try:
            model_hash = func() 
        except Exception:
            print("\tAn unexpected exception occurred when trying to get the model hash")
            print("\tusing internal method {}! ".format(func.__name__))
            print("\tError output:\n\t{} \n\t Trying other methods...".format(traceback.format_exc()))

        # break from loop if a method was successful
        if (model_hash is None) or (not model_hash):
            continue
        else:
            break

    if (model_hash is None) or (not model_hash):
        print("\tUnable to determine the model hash for {}".format(runid))
        if production:
            # production conversions need this meta data!
            except_str = "Model hash information required for production conversions!"
            raise Exception(except_str)
        else:
            print("\tCanESM hash set to 'Unknown'")
            model_hash = "Unknown"
    return model_hash

def parse_exptable(exptable):
    """
        parse the pipe (``|``) delimited experiment table turning it into a nested dictionary
        where the first key represents the runid.

        Parameters
        ----------
            exptable : str
                absolute location of the pipe delimieted experiment table

        Returns
        -------
            expdict : dict
                dictionary of the data contained in exptable
    """
    #--- get experiment table
    with open(exptable,'r') as f:
        table = f.readlines()

    #--- parse out column names
    # skip first and last entries in split list, which are null entries
    cnames_tmp = table[0].strip().split('|')[1:-1]
    cnames = [ cname.strip() for cname in cnames_tmp ]

    #--- create experiment dictionary
    expdict = {}
    for row in table[1:]:
        runid_dict      = {}
        row_entries_tmp = row.strip().split('|')[1:-1]
        row_entries     = [ entry.strip() for entry in row_entries_tmp ]

        # if row in empty, skip
        if not row_entries: continue

        # loop over row entries and create dictionary for desired runid
        for key,entry in zip(cnames,row_entries):
            # set runid as dictionary key
            if key == 'Run ID':
                runid = entry
            runid_dict[key] = entry

        # insert into expdict
        expdict[runid] = runid_dict
    return expdict

def conv_str2date(date):
    """
        take in a string and convert to ``cftime.datetime`` object. Accepts dates seperated by ``-`` or
        ``:``.

        .. note::

            - this was written because we want to use cftime, which is the time package used
              by the maintainers of netCDF (Unidata), but, as of 2019-01-19, it doesn't have
              a function to do this.

            - the ``cftime.datetime`` object has a value for ``dayofwk`` and ``dayofyr``, and through
              our construction here, it populates them with default values of -1 and 1. It is
              not clear what they do. Nevertheless, they don't seem to affect our date calcs.

        Parameters
        ----------
            date : str
                string to be converted

        Returns
        -------
            otp_date : ``cftime.datetime``
                converted output date
    """

    # set default error string
    err_str = "Unsupported date format! {}".format(date)

    # enforce formatting requirements
    if "-" in date and ":" in date:
        raise ValueError(err_str)
    elif "-" in date:
        delim = "-"
    elif ":" in date:
        delim = ":"
    else:
        raise ValueError(err_str)
    splt_date = date.split(delim)

    if len(splt_date) < 3:
        err_str += "\n\t Expecting a date with year, month, and day"
        raise ValueError(err_str)
    if len(splt_date) > 4:
        err_str += "\n\t Too many fields given. conv_str2date only accepts values for years, months, days, and hours"
        raise ValueError(err_str)
    if len(splt_date) == 3:
        splt_date.append("00")
    for val,var in zip(splt_date,['year','month','day','hour']):
        assert not "." in val,"Expecting an integer for {}".format(var)

    # extract values and assert expectations
    yr = int(splt_date[0])
    mn = int(splt_date[1])
    dy = int(splt_date[2])
    hr = int(splt_date[3])
    assert yr > 0,"Years must be positive"
    assert 1 <= mn <= 12,"Months must be within 1-12"
    assert 1 <= dy <= 31,"Days must be within 1-31"
    assert 0 <= hr <= 23,"Hr must be within 0-23"

    # FINALLY construct cftime datetime object
    otp_date = cftime.datetime(yr,mn,dy,hr)
    return otp_date

def deduce_conversion_realm(var_dat):
    """ Determine the "conversion realm" from the information contained in ``var_dat``.

        Take in dictionary of data associated with variables and deduce the
        'conversion' realm. In most cases the conversion realm will match the
        modeling realm, but if text is present in the ``CCCma target
        horizontal/vertical grid`` fields, then these data take precedence.

        .. note:: 

            This function assumes that the information in the
            ``CCCma target horizontal/vertical grid`` fields follows the
            following format:
                
                ``ocean_*``, or ``atmos_*``

            where the first field in a ``_`` seperated string represents the
            "conversion realm", which can be ``ocean`` or ``atmos``

        Parameters
        ----------
            var_dat : dict
                dictionary of CMOR table data associated with the variable of interest

        Returns
        -------
            conv_realm : str
                the resulting conversion realm (``ocean``/``atmos``)
    """

    # define conversion realms
    conv_realms = ['ocean','atmos']

    # group modeling realms
    orealms = ['ocean', 'seaIce', 'ocnBgchem', 'ocean seaIce',
                'seaIce ocean']

    arealms = ['atmos', 'land', 'atmos land', 'landIce',
                'land landIce', 'landIce land', 'aerosol',
                'atmos atmosChem', 'atmosChem']

    #=====================
    # get target grid text
    #=====================
    if var_dat['CCCma target horizontal grid']:
        horz_conv_realm = var_dat['CCCma target horizontal grid'].strip()   # strip whitespace
        horz_conv_realm = horz_conv_realm.split("_")[0]                     # pull out conversion grid
    else:
        horz_conv_realm = None

    if var_dat['CCCma target vertical grid']:
        vert_conv_realm = var_dat['CCCma target vertical grid'].strip()     # strip whitespace
        vert_conv_realm = vert_conv_realm.split("_")[0]                     # pull out conversion grid
    else:
        vert_conv_realm = None

    #=============
    # deduce realm
    #=============
    modeling_realm = var_dat['modeling_realm']
    if horz_conv_realm and vert_conv_realm:
        # assert conversion realms are consistent before assigning
        if not horz_conv_realm == vert_conv_realm:
            raise Exception("CCCma horz/vert target grids have inconsistent conversion realms!")
        # make sure the set realm is a possible conversion realms
        if any(rlm == horz_conv_realm for rlm in conv_realms):
            conv_realm = horz_conv_realm
    elif horz_conv_realm and any(rlm == horz_conv_realm for rlm in conv_realms):
        conv_realm = horz_conv_realm
    elif vert_conv_realm and any(rlm == vert_conv_realm for rlm in conv_realms):
        conv_realm = vert_conv_realm
    else:
        if modeling_realm in arealms:
            conv_realm = 'atmos'
        elif modeling_realm in orealms:
            conv_realm = 'ocean'
        else:
            raise Exception("{} : unknown realm".format(modeling_realm))

    return conv_realm

def load_retracted_variables( basefile = None, fnames = None ):
    '''Load information about retracted variables

    Parse logfile from the ESGF server (currently made by a cronjob) that keeps track of all the variables which have
    been retracted by CCCma at CMIP6. A pandas dataframe will be returned from this function which can then be queried
    later to figure out whether a new version number should be made.

    Parameters
    ----------
        basefile : str **optional**
            Path to a file containing retracted variables already in a pandas digestible
            format. (This was created by using a similar version of this function from the
            raw logfiles).
        fnames : list str **optional**
            A list of filenames that could be multiple logfiles from the ESGF server
            This is expected to be a file delimited by '|' with the headers given
            by the second row (index 1), two footer rows.

    Returns
    -------
        df : ``pandas dataframe``
            The pandas dataframe containing all information about the retracted variables

    Raises
    ------
        Exception
            If both parameters have been set to ``None``
    '''

    if basefile is None and fnames is None:
        raise Exception("basefile and fnames cannot both be none")

    def clean(self):
      '''Clean a pandas dataframe

      This will strip out any 'Unnamed' columns and make sure that all whitespace is deleted from column
      names and column entries
      '''

      for column in self.columns:
        # Strip whitespace if the data in a column is as tring
        if (type(self.loc[0,column]) == str):
          self[column] = self[column].apply( str.strip )
        # Do the same for the column name
        self.rename( columns = { column:column.strip() }, inplace=True)

      self = self.loc[:, ~self.columns.str.contains('^Unnamed')]
      return self

    # Add the new method to the Pandas dataframe
    pd.DataFrame.clean = clean

    # Define the format for the ESGF log
    varfile_d = {
         'skiprows'   : [0,2],
         'sep'        : '|',
         'header'     : 0,
         'skipfooter' : 2,
         'engine'     : 'python'
        }
    # Create the retracted variable pandas dataframe from various files
    if basefile is not None:
        if not os.path.exists( basefile ):
            raise Exception('File containing retracted variables not found: ' + basefile)
        base_df = pd.read_json( basefile )
    if fnames is not None:
        for varfile in fnames:
            if not os.path.exists( varfile ):
                raise Exception('File containing retracted variables not found: ' + varfile)
        fname_df = pd.concat( [pd.read_csv(varfile, **varfile_d) for varfile in fnames], ignore_index=True )
        fname_df.drop_duplicates( inplace = True)
        fname_df.clean()
        # Create some columns based on the filename
        fname_df['Source ID'] = pd.Series([ list(filter( lambda x: 'CanESM5' in x, name.split('.')))[0]
                                            for name in fname_pd.loc[:,'name'] ],index=fname_df.index)
        fname_df['varname']   = pd.Series([ varname.split('.')[-2]
                                            for varname in fname_df['name']],index=fname_df.index)
        fname_df['table']     = pd.Series([ varname.split('.')[-3]
                                            for varname in fname_df['name']],index=fname_df.index)
    if basefile is not None and fnames is not None:
        return pd.concat( [base_df, fname_df], ignore_index = True ).drop_duplicates()
    if fnames is not None and basefile is None:
        return fname_df
    if basefile is not None and fnames is None:
        return base_df

def check_version_format(version):
    '''Return True if version string is in the correct format.

    Examples of version strings in the correct format:
      "v20190306" (for 6 Mar 2019)
      "v20190429" (for 29 Apr 2019)

    Unlike all other parameters that make up dataset names, the version string
    is not part of the CMIP6 controlled vocabulary. Hence there's nothing in
    the data conversion pipeline to stop a version string being "this_version"
    or whatever. But CMIP6 guidance requests that version be a string of the
    form "v" followed by the date given as YYYYMMDD. This is why a check on the
    version string format is useful when running pycmor in production mode.
    '''
    if not isinstance(version, (str, unicode)):
        raise Exception('version should be a string')
    if not ( len(version) == 9 and version.startswith('v') and version.count('v') == 1 ):
        raise Exception('version={} has incorrect format. Example of correct format: version="v20190429"'.format(version))
    version_date = version.strip('v')
    try:
        version_date = int(version_date)
    except:
        raise Exception('date in the version string should be an integer, e.g. 20190429')
    month = int(version.strip('v')[4:6])
    day =   int(version.strip('v')[6:8])
    if month < 1 or month > 12:
        raise Exception('version={}: month out of range'.format(version))
    if day < 1 or day > 31:
        raise Exception('version={}: day out of range'.format(version))
    return version_date

def set_var_version( var, table, expinfo, retracted_df, default_version, new_version ):
    '''Return the version string to use for the given variable

    Most variables will have the same version number. However, if a variable has been retracted the version should be
    set to new_version

    Parameters
    ----------
        var : str
            The CMOR name of the variable being converted
        table : str
            The CMOR table of the variable being converted
        runinfo : dict
            Dictionary containing the parsed experiment info table for the given runid
        retracted_df : ``pandas dataframe``
            Dataframe with information about the retracted variables
        default_version : str
            The default version string to use for non-retracted variables
        new_version : str
            The version to use for variables which have been previously retracted

    Returns
    -------
        version : str
            The string that the variable will be used

    Raises
    ------
        Exception 
            If it is found that ``var`` has been retracted, yet ``new_version``
            was set to an empty string.
    '''

    # Get the date (an integer, e.g. 20190429) of the default version string.
    # This also ensures that the default version string is formatted correctly.
    default_version_date = check_version_format(default_version)

    # Build the search criteria to find the variable in the table
    criteria = (
            (retracted_df['experiment_id'] == expinfo['Experiment ID']) &
            (retracted_df['Source ID']     == expinfo['Source ID']    ) &
            (retracted_df['member_id']     == expinfo['Variant Label']) &
            (retracted_df['varname']       == var                     ) &
            (retracted_df['table']         == table                   )
            )

    df = retracted_df.loc[criteria]
    if len(df) > 0:
        # This dataset has been retracted before.
        # If the default version is later than the latest retracted version
        # then use the default version.
        # Otherwise a new version must be specified.

        # Get info on the retracted versions.
        #  retracted_versions_date: integers giving dates of retracted versions (e.g. 20190306)
        #  retracted_versions: version strings corresponding to those integers (e.g. 'v20190306')
        retracted_versions_date = [df.iloc[k].version for k in range(len(df.index))]
        retracted_versions_date = list(set(retracted_versions_date)) # remove any duplicates (shouldn't be any, but just to be sure)
        retracted_versions = ['v{}'.format(m) for m in retracted_versions_date]
        # Ensure that retracted versions have the expected format
        l = [check_version_format(s) for s in retracted_versions]
        if l != retracted_versions_date:
            dataset=df.name.iloc[0] # should be the same for all datasets in df
            raise Exception('Unexpected version format for retracted dataset={}'.format(dataset))

        latest_retracted_date = max(retracted_versions_date)
        print('WARNING: Retracted variable {}_{} found. Latest retracted version: {}'\
            .format(var, table, latest_retracted_date))

        # Decide whether we can use default_version or need to go to new_version
        if default_version_date > latest_retracted_date:
            # The default version is later than the latest retracted version, so it's ok to use.
            print('Default version ({}) is later than the latest retracted version. Default version (-v option of pycmor.py) will be used.'\
                .format(default_version))
            return default_version
        else:
            # The new version should be used.
            print('Default version ({}) is not later than the latest retracted version. New version (-n option of pycmor.py) will be used.'\
                .format(default_version))
            # Check that the proposed new version is valid for this variable.
            if (new_version == ''):
                raise Exception('New version needs to be set in callcp.sh (-n option for pycmor.py), which should be later than {}'\
                    .format(latest_retracted_date))
            new_version_date = check_version_format(new_version) # this also ensures new_version is formatted correctly
            if new_version_date in retracted_versions_date:
                raise Exception('new_version={} has already been used for a retracted version of this variable'.format(new_version))
            if new_version_date <= latest_retracted_date:
                raise Exception('new_version={} has an earlier date than the latest retracted version of this variable ({})'\
                    .format(new_version, latest_retracted_date))
            #print('WARNING: Retracted variable {}_{} found (latest retracted version, {}, is later than the default version, {}). Setting version to \'{}\'.'\
            #    .format(var, table, latest_retracted_date, default_version_date, new_version))
            return new_version

    else:
        # This dataset has not been retracted before, so use the default version.
        return default_version

def check_repo():
    """ Verify that the repository is clean, and throw exception if not.

    Raises
    ------
    Exception
        If uncommitted changes are found in the working :code:`ncconv` repository.
    """
    # store current working dir and navigate into repo
    cwd = os.getcwd()
    os.chdir(os.environ['NCCONV_DIR'])

    # run git status --porcelain and check for any output
    cmd = [
            'git',
            'status',
            '--porcelain'
          ]
    output = subprocess.check_output(cmd).strip()
    os.chdir(cwd)
    if output:
        print(output)
        raise Exception("There are uncommitted changes in the repository!")
    else:
        print("\tRepo clean")


def run_optional_deck(optdeck, tsvars_L, diagfiles, pfx, conv_realm, 
                        out_suff='derived', wrk_dir='optional_deck_wrk', nemo_mask=None):
    """Spawn a subprocess to run the optional deck on the given input variable/file lists. 

        Parameters
        ----------
            optdeck : str
                optional deck to run (**Note:** the executable binary must be in
                :code:`ncconv/bin`).
            tsvars_L : list of str
                variables being operated on
            diagfiles : list of str
                filetypes containing the variables in :code:`tsvars_L`.  If
                :code:`len(diagfiles) > 1`, it **must** be the same length as
                :code:`tsvars_L`. If :code:`len(diagfiles) == 1`, the function
                assumes all variables in :code:`tsvars_L` are found in the same
                filetype.
            pfx : str
                prefix found in **all** input files, before the filetype (ex. :code:`sc_{runid}_{chunk}`)
            conv_realm : str
                identifies what realm the input variables belong to ('ocean' or 'atmos')
            out_suff : str, **optional**
                text that is used to define the output file in combination with
                :code:`pfx`. Defaults to :code:`derived`.
            wrk_dir : str **optional**
                path to the optional deck scratch space. **Gets cleaned if exists
                prior to execution, and is cleaned on completion or Exception.**
                Defaults to :code:`optional_deck_wrk`.
            nemo_mask : str **optional**
                if ``conv_realm == 'ocean'``, this must be given in order to link in the mask file
                to the work directory 

        Returns
        -------
            success : bool 
                :code:`True` if optional deck runs successfully, ``False`` otherwise.
            outfile : str
                Name of the produced file.
            logtxt : str
                Error text associated with running the optional deck.
    """
    print("Applying optional deck: {}".format(optdeck))
    cwd = os.getcwd()

    # set up tmp working directory
    if os.path.exists(wrk_dir): shutil.rmtree(wrk_dir)
    os.mkdir(wrk_dir)
    os.chdir(wrk_dir)
    if conv_realm == 'ocean':
        os.symlink(os.path.join(cwd,nemo_mask),"nemo_mesh_mask")

    # build argument list
    if len(diagfiles) > 1:
        # input files in multiple diag files, must send in multiple files to deck
        #   - the assumed arg format for these decks is
        #       file1 var1 file2 var2 .... fileN varN outputf
        args = [optdeck]
        for df,vr in zip(diagfiles,tsvars_L):
            f = "{pfx}_{diagfile}_{var}".format(pfx=pfx,diagfile=df,var=vr)
            args.extend([f,vr])
        outfile = "{}_mixed_{}".format(pfx,out_suff)
        args.append(outfile)
    else:
        # only one diag file needed, use standard prefix
        pfx += "_{diagfile}".format(diagfile=diagfiles[0])
        args = [optdeck, pfx]
        for v in tsvars_L: args.append(v)       # add input variable - must be in list format
        args.append(out_suff)                   # add output suffix
        outfile = "{}_{}".format(pfx,out_suff)  # filename of the output produced by the optional deck

    # call optional deck and verify that the output file was created
    try: 
        # Run deck, piping stdout to stderr for logging purposes
        subprocess.check_output(args,stderr=subprocess.STDOUT)
        if not os.path.exists(outfile):
            logtxt  = "{} ran without producing a non-zero exit status, ".format(optdeck)
            logtxt += "but failed to produce the desired output. Check optional deck for errors or bugs..\n"
            logtxt += "Attempted command: \n\t{}\n".format(" ".join(args))
            success = False
            outfile = None
        else:
            logtxt   = "Successfully ran optional deck {}\n".format(optdeck)
            logtxt  += "Executed command:\n\t{}\n".format(" ".join(args))
            success  = True
            shutil.move(outfile, os.path.join(cwd,outfile))
    except subprocess.CalledProcessError as e:
        logtxt  = "{} returned a non-zero exit status\n".format(optdeck)
        logtxt += "Attempted command:\n\t{}\n".format(" ".join(args))
        logtxt += "Optional Deck Output:\n{}\n".format(e.output)
        success = False
        outfile = None
    except OSError as e:
        logtxt  = "Failed to run {}\n".format(optdeck)
        logtxt += "Be sure to confirm that the deck is executable, in the ncconv bin directory\n"
        logtxt += "Error output:\n{}\n".format(traceback.format_exc())
        success = False
        outfile = None
    finally: 
        # regardless of success or not, navigate back and clean work space
        os.chdir(cwd)
        shutil.rmtree(wrk_dir)
    return success, outfile, logtxt
