#!/bin/bash
usage() {
    echo -e  "Usage: env-setup.sh [-f -h]"
    echo -e  "\tPurpose: Setup a conda environment for a standard cmor environment"
    echo -e  "\tOptions:"
    echo -e  "\t\t-f   ...Remove the current 'cmor' envirionment and reinstall from scratch"
    echo -e  "\t\t-h   ...Display this usage"
    exit
}

FORCE_REINSTALL=0

while getopts ":hf" opt; do
    case ${opt} in
        h ) usage ;;
        f ) FORCE_REINSTALL=1 ;;
        /? ) echo "Invalid option"
             usage
            ;;
    esac
done
SCRIPTPATH="$(dirname -- "$(readlink -f -- "$0")")"

# check for conda acces
which conda ||
{ 
echo "conda is not available!"
echo "conda must be available to setup the environment!"
echo "... bailing on environment setup"
exit 1
}

# find environment yaml file
YML_PATH=$(find . -name cmor.yml)
echo $YML_PATH
ENV_NAME=$(grep "name:" $YML_PATH | awk '{print $2}')
echo "Configuring $ENV_NAME"

conda config --set ssl_verify False
# Remove the current CMOR environment if requested
if [ $FORCE_REINSTALL -eq 1 ]; then
    conda env remove -y -n $ENV_NAME
fi

# Check to see if we can load the CMOR environment
# If we can't then install the CMOR environment and then load it
{
    echo "Activating $ENV_NAME"
    source activate $ENV_NAME > /dev/null 2>&1
} ||
{
    # basic setup
    echo "$ENV_NAME environment not found, installing..."
    conda env create -f $YML_PATH
    source activate $ENV_NAME

    # install 3.1.0 cf checker
    wget https://github.com/cedadev/cf-checker/archive/3.1.0.tar.gz
    tar -zxf 3.1.0.tar.gz
    cd cf-checker-3.1.0
    python setup.py install
    cd ..
    rm -rf cf-checker-3.1.0 3.1.0.tar.gz
}
