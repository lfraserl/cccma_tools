#!/usr/bin/env python
'''
Sets 'version' attribute of the output path, which is the last component of the CMIP6 output path. 
E.g. in the output path

  CMIP6/CMIP/CCCma/CanESM5/piControl/r1i1p1f1/Amon/tas/gn/v20190127/

"v20190127" is the version. In the cccma_user_input.json file the components of the output path are specified by the attribute

  "output_path_template": "<mip_era><activity_id><institution_id><source_id><experiment_id><_member_id><table><variable_id><grid_label><version>"

The version is supposed to indicate a representative approximate date for the version of the data. 
For more info see

  https://www.earthsystemcog.org/site_media/projects/wip/CMIP6_global_attributes_filenames_CVs_v6.2.6.pdf

This script creates a file called .dataset_version containing a version string indicating the date 
on which it's created. It should be called by config-pycmor and runs in the current working dir. 
It does two things:

1. Creates the .dataset_version file if it doesn't already exist. Puts this file in the pycmor code dir 
(ncconv/bin/pycmor). This file isn't tracked by git, so a new version of this file will be created the 
first time this script is run for a given  instance (clone) of the pycmor code. 

2. Inserts a parameter version=[the version string in .dataset_version] into the callcp.sh script
into current the working dir. 

As long as the .dataset_version file persists, the same version string will appear by default in callcp.sh
each time a new working dir is created. The user still can modify the version= statement in callcp.sh if 
desired. And the default version in .dataset_version needs to be changed, the user can manually edit the 
.dataset_version file (or delete it so that a new one will be created the next time config-pycmor is run).

'''
import datetime
import os
if 'NCCONV_DIR' in os.environ:
    pycmor_dir = os.path.join( os.environ['NCCONV_DIR'], 'bin/pycmor' )
else:
    pycmor_dir = '../bin/pycmor/'
version_file = '.dataset_version'
version_filepath = os.path.join(pycmor_dir, version_file)
try:
    if not os.path.exists(version_filepath):
        # If file indicating the version doesn't exist, create it.
        # Version string should be of form 'vYYYYMMDD', indicating the approximate date of the version.
        version = 'v' + datetime.datetime.now().strftime('%4Y%2m%2d')
        with open(version_filepath,'w') as f:
            f.write('version=\'' + version + '\'')
    # Read the version from version_file.    
    with open(version_filepath,'r') as f:
        version = f.read().strip()
    # Modify callcp.sh to set version = the version specified in version_file.
    with open('callcp.sh','r') as f:
        w = f.read()
    lw = w.split('\n')
    for k,s in enumerate(lw):
        if s.strip().startswith('version='):
            lw[k] = version
            break
    with open('callcp.sh','w') as f:
        f.write('\n'.join(lw))
except:
    pass

