
Compliance Checking
-------------------
As laid out in section 6 of the `CMIP6 participation guidelines for modelers
<https://pcmdi.llnl.gov/CMIP6/Guide/modelers.html>`_, software called ``PrePARE``
and ``cfchecks`` will be used to test compliance of netcdf data prior to
publication and "**only files passing all tests will be published and made available for
download**".

It is currently assumed that if data is produced from ``pycmor``, without any 
errors, it can be published on the ESGF server, and as of 2019-04-24, there has 
been no reason to think otherwise. Nevertheless, some users may want to test
their data to see if it is "compliant", so we created a ``python`` module
called ``compliance_checks.py``, that can run files through the *predicted* 
compliance pipeline. 

.. note::
    
    This software tool hasn't been extensively used or tested, as we have 
    been able to publish on the ESGF without it.

The intended use of this software is to be called from the command line and run
a set of already converted data; if you run ``compliance_checks.py -h`` you
will see this usage message

.. code-block:: text

    usage: compliance_checks.py [-h] (-f FILES [FILES ...] | -d DIRECTORY)
                                [--logfile LOGFILE]
                                [--table_path PATH2_CMIPCMOR_TABLES] [--list_wrns]
                                [--list_errs]

    Check ESGF compliance of netcdf files.

    optional arguments:
      -h, --help            show this help message and exit
      -f FILES [FILES ...], --files FILES [FILES ...]
                            space delimited list of files to check for ESGF
                            compliance
      -d DIRECTORY, --directory DIRECTORY
                            directory containing files to check for ESGF
                            compliance
      --logfile LOGFILE     name of log file to store PrePARE and cfcheck output.
                            Defaults to 'compliance.log'
      --table_path PATH2_CMIPCMOR_TABLES
                            path to cmip cmor tables. Defaults to 'cmip6-cmor-
                            tables/Tables'
      --list_wrns           make compliance_checks.py output the list of files
                            causing cfcheck warnings
      --list_errs           make compliance_checks.py output the list of files
                            causing errors (both from PrePARE and cfcheck)

As is alluded in the usage text, this code is capable of accepting a list of
files or a directory, depending on if the ``-f`` or ``-d`` flag is given. If the
``-d`` flag (for directory) is given, then ``compliance_checks.py`` will
recursively search for **ALL** ``*.nc`` files, and run those through the
compliance pipeline. 

As an example, if we want to use the directory functionality to see how many
netcdf files within our ``CMIP6`` directory pass the pipeline, we could run
``compliance_checks.py -d CMIP6``, and get the following summary

.. code-block:: text

    Files Inspected:                38
    Files with PrePARE errors:      11
    Files with cfcheck errors:      0
    Files with cfcheck warnings:    36

    See compliance.log for specifics on warnings/errors

Note that both ``PrePARE`` and ``cfchecks`` can produce quite a lot of output,
especially if a large amount of files are given to ``compliance_checks.py``. To
avoid barrying the user in this output, ``compliance_checks.py`` catches the
``stderr`` and ``stdout`` from these tools, and writes them to a log file, which by
default is called ``compliance.log`` - **note** a different file can be specified
with the ``--logfile`` flag. 

Now, if a user would like to see explicitly what files are causing errors or
warnings, they can add the ``--list_wrns`` or ``--list_errs`` flags. For example,
in the above example ,if ``--list_errs`` is added, the script would output

.. code-block:: text

    Files Inspected:                38
    Files with PrePARE errors:      11
    Files with cfcheck errors:      0
    Files with cfcheck warnings:    36

    Files causing errors:
           PrePARE:
                   CMIP6/CMIP/CCCma/CanESM5/piControl/r1i1p1f1/Oyr/opottemptend/gn/v20190213/opottemptend_Oyr_CanESM5_piControl_r1i1p1f1_gnm0BgM6178314.nc
                   CMIP6/CMIP/CCCma/CanESM5/piControl/r1i1p1f1/Oyr/opottemppmdiff/gn/v20190213/opottemppmdiff_Oyr_CanESM5_piControl_r1i1p1f1_gn2wB7MF178314.nc
                   CMIP6/CMIP/CCCma/CanESM5/piControl/r1i1p1f1/Oyr/osaltpmdiff/gn/v20190213/osaltpmdiff_Oyr_CanESM5_piControl_r1i1p1f1_gncFs877178314.nc
                   CMIP6/CMIP/CCCma/CanESM5/piControl/r1i1p1f1/Oyr/osaltrmadvect/gn/v20190213/osaltrmadvect_Oyr_CanESM5_piControl_r1i1p1f1_gnqCOkTo178314.nc
                   CMIP6/CMIP/CCCma/CanESM5/piControl/r1i1p1f1/Oyr/osaltdiff/gn/v20190213/osaltdiff_Oyr_CanESM5_piControl_r1i1p1f1_gnsWMYqZ178314.nc
                   CMIP6/CMIP/CCCma/CanESM5/piControl/r1i1p1f1/Oyr/opottemppadvect/gn/v20190213/opottemppadvect_Oyr_CanESM5_piControl_r1i1p1f1_gnnVwYI7178314.nc
                   CMIP6/CMIP/CCCma/CanESM5/piControl/r1i1p1f1/Oyr/osalttend/gn/v20190213/osalttend_Oyr_CanESM5_piControl_r1i1p1f1_gnwEMlYI178314.nc
                   CMIP6/CMIP/CCCma/CanESM5/piControl/r1i1p1f1/Oyr/rsdoabsorb/gn/v20190213/rsdoabsorb_Oyr_CanESM5_piControl_r1i1p1f1_gnf4sUtX178314.nc
                   CMIP6/CMIP/CCCma/CanESM5/piControl/r1i1p1f1/Oyr/osaltpadvect/gn/v20190213/osaltpadvect_Oyr_CanESM5_piControl_r1i1p1f1_gnVgAPIZ178314.nc
                   CMIP6/CMIP/CCCma/CanESM5/piControl/r1i1p1f1/Oyr/opottemprmadvect/gn/v20190213/opottemprmadvect_Oyr_CanESM5_piControl_r1i1p1f1_gnfEwMum178314.nc
                   CMIP6/CMIP/CCCma/CanESM5/piControl/r1i1p1f1/Oyr/opottempdiff/gn/v20190213/opottempdiff_Oyr_CanESM5_piControl_r1i1p1f1_gncDXS1n178314.nc

    See compliance.log for specifics on warnings/errors

It should be noted that we only expect files that throw actual errors from
these programs will fail; warnings are expected. It should also be noted that
``PrePARE`` is cooked right in the ``CMOR`` software itself, so it is expected
that if our data gets through ``CMOR``, it will also pass ``PrePARE``. This
software seems to be meant for users who don't use the ``CMOR`` software to
prepare their data. Therefore we don't expect files that are produced from
``pycmor``, without ``CMOR`` errors, to result in any ``PrePARE`` errors.
The ``cfchecks`` software however, is built separately, and
meant to check for CF-compliance, and it seems possible that errors could
arise from this software. Nevertheless, this software hasn't been tested
thuroughly, so further investigation would be required to look into this.
