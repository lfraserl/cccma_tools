      module misc_param
      implicit none

c.....Root directory for CCCma source code etc
      character(256) :: ccrnsrc="whocares"

c.....Location of udunits.dat
      character(256) :: udunits_data_path=
     & "/home/scrd102/cccma_libs/udunits-1.12.11/etc/udunits.dat"
      namelist /nc2ccc_par/ ccrnsrc, udunits_data_path

      end module misc_param
